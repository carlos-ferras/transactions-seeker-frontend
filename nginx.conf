server {
  listen 80;

  # send headers in one peace
  tcp_nopush on;
  # allow the server to close connection on non responding client, this will free up memory
  reset_timedout_connection on;
  # request timed out -- default 60
  client_body_timeout 20;
  # if client stop responding, free up memory -- default 60
  send_timeout 20;
  # server will close connection after this time -- default 75
  keepalive_timeout 40;
  # reduce the data that needs to be sent over network
  gzip on;
  gzip_min_length 200;
  gzip_types
    text/plain
    text/css
    text/xml
    text/javascript
    application/javascript
    application/x-javascript
    application/json
    application/x-web-app-manifest+json
    application/xml
    application/xhtml+xml
    application/x-font-ttf
    application/octet-stream
    font/opentype
    image/x-icon
    image/svg+xml;
  gzip_proxied expired no-cache no-store private auth;
  gzip_http_version 1.1;
  gzip_comp_level 5;
  gzip_vary on;
  gzip_disable msie6;

  location /api/graphql {
      proxy_pass http://TS_API_IP:TS_API_PORT/graphql/;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
      proxy_set_header Host $host;
      proxy_set_header X-Forwarded-For $remote_addr;
  }

  location /api {
    proxy_pass http://TS_API_IP:TS_API_PORT/;
  }

  location / {
    alias /usr/share/nginx/html/transactions-seeker/;
    try_files $uri $uri/ /index.html;
  }
}

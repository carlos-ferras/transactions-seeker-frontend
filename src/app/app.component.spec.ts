import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material';

import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { configureTestSuite } from 'ng-bullet';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { provideMagicalMock, Mock } from '~app/test-utils';
import { SeoService, SplashScreenService } from '~core/services';
import { AppComponent } from './app.component';

const STORE_STATE = {
  routing: {
    currentRoute: {
      urlAfterRedirects: '/'
    }
  },
  appSettings: {
    isPageLoading: false
  }
};

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let splashScreenService: Mock<SplashScreenService>;
  let seoService: Mock<SeoService>;
  let swUpdate: Mock<SwUpdate>;
  let store: MockStore<any>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        provideMagicalMock(SplashScreenService),
        provideMagicalMock(SeoService),
        provideMagicalMock(MatDialog),
        provideMagicalMock(SwUpdate),
        provideMockStore({
          initialState: STORE_STATE
        }),
        {
          provide: 'window',
          useValue: {
            location: {
              reload: jest.fn()
            }
          }
        }
      ],
      declarations: [AppComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);
    splashScreenService = TestBed.get(SplashScreenService);
    seoService = TestBed.get(SeoService);
    swUpdate = TestBed.get(SwUpdate);
    // @ts-ignore
    swUpdate.available = of(true);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.showLoadingBar).toBeFalsy();
    expect(splashScreenService.hide).toHaveBeenCalled();
    expect(seoService.extractRouteData).toHaveBeenCalled();
  });

  it('should update loading status', () => {
    jest.spyOn(component, 'setLoadingStatus');
    STORE_STATE.appSettings.isPageLoading = true;
    store.setState(STORE_STATE);
    expect(component.showLoadingBar).toBeTruthy();
    expect(component.setLoadingStatus).toHaveBeenCalledWith(true);
  });

  it('should extract route seo data when url change', () => {
    STORE_STATE.routing.currentRoute.urlAfterRedirects = '/home';
    store.setState(STORE_STATE);
    expect(seoService.extractRouteData).toHaveBeenCalled();
  });
});

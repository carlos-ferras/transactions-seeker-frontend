import { HttpClient } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';

import {
  MissingTranslationHandler,
  MissingTranslationHandlerParams,
  TranslateCompiler,
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateMessageFormatCompiler } from 'ngx-translate-messageformat-compiler';
import 'hammerjs';

import { environment } from '~env/environment';
import { RootStoreModule } from '~core/store';
import { AppUpdateDialogModule } from '~features/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { CoreModule } from './core';

export function appInitFactory(appService: AppService): () => Promise<any> {
  return () => appService.initializeApp();
}

export function windowFactory() {
  return window;
}

export function TranslateLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export class CustomMissingTranslationHandler implements MissingTranslationHandler {
  handle(params: MissingTranslationHandlerParams) {
    return params.key;
  }
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    RootStoreModule,
    AppUpdateDialogModule,
    CoreModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: TranslateLoaderFactory,
        deps: [HttpClient]
      },
      compiler: {
        provide: TranslateCompiler,
        useClass: TranslateMessageFormatCompiler
      },
      missingTranslationHandler: {
        provide: MissingTranslationHandler,
        useClass: CustomMissingTranslationHandler
      }
    }),
    ServiceWorkerModule.register(
      'ngsw-worker.js',
      {
        enabled: environment.production
      }
    )
  ],
  providers: [
    AppService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitFactory,
      deps: [AppService],
      multi: true
    },
    {
      provide: 'window',
      useFactory: windowFactory
    },
    {
      provide: 'medias',
      useValue: {
        mobile: 600,
        tabletPortrait: 800,
        tabletLandscape: 1280,
        desktop: 1366,
        desktopHD: 1680,
        desktopWSXGA: 1920,
        desktopFHD: 2048
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

import { Injectable, Injector } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

import { Store } from '@ngrx/store';

import { environment } from '~env/environment';
import { AppSettingsActions } from '~core/store';

@Injectable()
export class AppService {
  store: Store<any>;
  iconSet = [
    'root',
    'child',
    'i18n',
    'sameGeoInfo',
    'sameEmail',
    'sameName',
    'samePhoneNumber',
    'sameDeviceToken',
    'sameDevice'
  ];

  constructor(private injector: Injector) {
  }

  initializeApp(): Promise<any> {
    this.store = this.injector.get(Store);

    return new Promise((resolve) => {
      this.initializeI18n();
      this.loadAppIcons();
      resolve();
    });
  }

  loadAppIcons() {
    const matIconRegistry = this.injector.get(MatIconRegistry);
    const domSanitizer = this.injector.get(DomSanitizer);
    this.iconSet.forEach((iconName: string) => {
      matIconRegistry.addSvgIcon(
        iconName,
        domSanitizer.bypassSecurityTrustResourceUrl(`../assets/images/icons/${iconName}.svg`)
      );
    });
  }

  initializeI18n() {
    if (environment['tsLangs']) {
      const langs = environment['tsLangs']
        .split('"')
        .join('')
        .split(' ')
        .map((lang) => {
          const code = lang.split(',')[0];
          const name = lang.split(',')[1];
          return {
            code,
            name
          };
        });

      this.store.dispatch(
        new AppSettingsActions.SetAvailableLangsAction({
          langs
        })
      );

      this.store.dispatch(
        new AppSettingsActions.SetDefaultLangAction({
          lang: environment['tsDefaultLang']
        })
      );
    }
  }
}

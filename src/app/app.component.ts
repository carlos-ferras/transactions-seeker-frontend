import { AfterViewInit, Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { SwUpdate } from '@angular/service-worker';
import { MatDialog } from '@angular/material';

import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';

import { SeoService, SplashScreenService } from '~core/services';
import { AppSettingsSelectors, RootStore, RoutingSelectors } from '~core/store';
import { AppUpdateDialogComponent } from '~features/common';

@Component({
  selector: 'ts-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  showLoadingBar: boolean;

  constructor(
    @Inject(DOCUMENT) private document,
    @Inject('window') private window: Window,
    private renderer: Renderer2,
    private route: ActivatedRoute,
    private swUpdate: SwUpdate,
    private dialog: MatDialog,
    private store: Store<RootStore.State>,
    private seoService: SeoService,
    private splashScreenService: SplashScreenService
  ) {
  }

  ngOnInit() {
    this.store.select(AppSettingsSelectors.selectIsPageLoading)
      .subscribe((state: boolean) => {
        this.showLoadingBar = state;
        this.setLoadingStatus(state);
      });

    this.swUpdate.available.subscribe(() => {
      this.askUserToUpdateApp();
    });
  }

  ngAfterViewInit() {
    this.splashScreenService.hide();

    this.store.select(RoutingSelectors.selectCurrentRoute)
      .subscribe(() => {
        this.seoService.extractRouteData(this.route.snapshot);
      });
  }

  askUserToUpdateApp() {
    const dialogRef = this.dialog.open(
      AppUpdateDialogComponent,
      {
        minWidth: '300px',
        maxWidth: '100%',
        width: '500px',
        height: 'auto'
      }
    );
    dialogRef
      .afterClosed()
      .pipe(take(1))
      .subscribe((shouldUpdate: boolean) => {
        if (shouldUpdate) {
          this.reloadPage();
        }
      });
  }

  reloadPage() {
    this.window.location.reload();
  }

  setLoadingStatus(state: boolean) {
    if (state) {
      this.renderer.addClass(this.document.body, 'page-loading');
    } else {
      this.renderer.removeClass(this.document.body, 'page-loading');
    }
  }
}

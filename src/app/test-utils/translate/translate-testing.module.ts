import { NgModule } from '@angular/core';

import { TranslateStubPipe } from './translate-stub.pipe';

@NgModule({
  declarations: [TranslateStubPipe],
  exports: [TranslateStubPipe]
})
export class TranslateTestingModule {}

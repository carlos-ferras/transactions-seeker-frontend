import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'ts-app-update-dialog',
  templateUrl: './app-update-dialog.component.html'
})
export class AppUpdateDialogComponent {
  constructor(
    private dialogRef: MatDialogRef<AppUpdateDialogComponent>
  ) {
  }

  close(shouldUpdate: boolean) {
    this.dialogRef.close(shouldUpdate);
  }
}

import { NgModule } from '@angular/core';

import { CoreModule } from '~app/core';
import { AppUpdateDialogComponent } from './app-update-dialog.component';

@NgModule({
  imports: [CoreModule],
  declarations: [AppUpdateDialogComponent],
  exports: [AppUpdateDialogComponent],
  entryComponents: [AppUpdateDialogComponent]
})
export class AppUpdateDialogModule {}

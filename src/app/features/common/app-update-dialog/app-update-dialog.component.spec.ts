import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material';

import { configureTestSuite } from 'ng-bullet';

import { provideMagicalMock, Mock, TranslateTestingModule } from '~app/test-utils';
import { AppUpdateDialogComponent } from './app-update-dialog.component';

describe('AppUpdateDialogComponent', () => {
  let component: AppUpdateDialogComponent;
  let fixture: ComponentFixture<AppUpdateDialogComponent>;
  let dialogRef: Mock<MatDialogRef<any>>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule],
      declarations: [AppUpdateDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMagicalMock(MatDialogRef)
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppUpdateDialogComponent);
    component = fixture.componentInstance;
    dialogRef = TestBed.get(MatDialogRef);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the dialog', () => {
    component.close(true);
    expect(dialogRef.close).toHaveBeenCalledWith(true);
  });
});

import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { takeUntil } from 'rxjs/operators';

import { TransactionsService } from '~core/services';
import { Transaction } from '~core/models';
import { RootStore, TransactionsActions } from '~core/store';

@Component({
  selector: 'ts-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private store: Store<RootStore.State>,
    private transactionsService: TransactionsService
  ) {
  }

  ngOnInit(): void {
    this.transactionsService.watchTransactionCreated()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((transaction: Transaction) => {
        this.store.dispatch(
          new TransactionsActions.SetTransactionAction({transaction})
        )
      });

    this.transactionsService.watchTransactionUpdated()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((transaction: Transaction) => {
        this.store.dispatch(
          new TransactionsActions.SetUpdatedTransactionAction({transaction})
        )
      });

    this.transactionsService.watchTransactionDeleted()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((transaction: Transaction) => {
        this.store.dispatch(
          new TransactionsActions.SetDeletedTransactionAction({transaction})
        )
      });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

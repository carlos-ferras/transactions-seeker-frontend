import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { debounceTime, delay, distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { RootStore, TransactionsActions } from '~core/store';

@Component({
  selector: 'ts-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit, OnDestroy {
  filtersForm: FormGroup;
  keyUpSubject: Subject<any> = new Subject<any>();
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<RootStore.State>
  ) {
  }

  ngOnInit(): void {
    this.filtersForm = this.formBuilder.group({
      transactionId: ['', [Validators.required]],
      confidenceLevel: [1, [Validators.min(0), Validators.max(1)]]
    });

    this.keyUpSubject
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        delay(90),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe(() => this.applyFilters());
  }

  applyFilters() {
    if (this.filtersForm.valid) {
      this.store.dispatch(
        new TransactionsActions.FilterTransactionsAction({
          filterValue: this.filtersForm.value
        })
      );
    }
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { configureTestSuite } from 'ng-bullet';
import { provideMockStore } from '@ngrx/store/testing';

import { Mock, provideMagicalMock } from '~app/test-utils';
import { TransactionsService } from '~core/services';
import { TransactionsComponent } from './transactions.component';
import { of } from 'rxjs';

describe('TransactionsComponent', () => {
  let component: TransactionsComponent;
  let fixture: ComponentFixture<TransactionsComponent>;
  let transactionsService: Mock<TransactionsService>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [TransactionsComponent],
      providers: [
        provideMockStore({}),
        provideMagicalMock(TransactionsService)
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionsComponent);
    component = fixture.componentInstance;

    transactionsService = TestBed.get(TransactionsService);
    transactionsService.watchTransactionCreated.mockReturnValue(
      of(true)
    );
    transactionsService.watchTransactionUpdated.mockReturnValue(
      of(true)
    );
    transactionsService.watchTransactionDeleted.mockReturnValue(
      of(true)
    );

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(transactionsService.watchTransactionCreated).toHaveBeenCalled();
    expect(transactionsService.watchTransactionUpdated).toHaveBeenCalled();
    expect(transactionsService.watchTransactionDeleted).toHaveBeenCalled();
  });
});

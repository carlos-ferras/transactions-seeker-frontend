import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { takeUntil } from 'rxjs/operators';
import { _ } from '@biesbjerg/ngx-translate-extract/dist/utils/utils';

import { AppSettingsSelectors, RootStore, TransactionsActions, TransactionsSelectors } from '~core/store';
import { Transaction } from '~core/models';
import { ConnectionInfoCellComponent } from '../connection-info-cell';

@Component({
  selector: 'ts-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit, OnDestroy {
  transactions: Transaction[];
  columnDefs = [];
  private translations: any;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private store: Store<RootStore.State>) {
  }

  ngOnInit(): void {
    this.store.dispatch(
      new TransactionsActions.LoadTransactionsAction()
    );

    this.store.select(TransactionsSelectors.selectFilteredTransactions)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((transactions: Transaction[]) => {
        this.transactions = transactions;
      });

    this.store.select(AppSettingsSelectors.selectTranslations)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((translations: any) => {
        this.translations = translations;
        this.updateColumnsDefinitions();
      });
  }

  updateColumnsDefinitions() {
    this.columnDefs = [
      {
        headerName: this.translations[_('Transaction ID') as string],
        field: 'id',
        resizable: true
      },
      {
        headerName: this.translations[_('Connection Info') as string],
        valueGetter: (params) => (params.data && params.data.connectionInfo) ? params.data.connectionInfo.type : '',
        resizable: true,
        cellRendererFramework: ConnectionInfoCellComponent
      },
      {
        headerName: this.translations[_('Confidence Level') as string],
        valueGetter: (params) => (params.data && params.data.connectionInfo) ? params.data.connectionInfo.confidence : '\t·\t·\t·',
        resizable: true
      },
      {
        headerName: this.translations[_('Name') as string],
        field: 'name',
        resizable: true
      },
      {
        headerName: this.translations[_('Email') as string],
        field: 'email',
        resizable: true
      },
      {
        headerName: this.translations[_('Phone') as string],
        field: 'phone',
        resizable: true
      },
      {
        headerName: this.translations[_('Age') as string],
        field: 'age',
        resizable: true
      }
    ];
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

import { Component } from '@angular/core';

import { AgRendererComponent } from 'ag-grid-angular';

@Component({
  selector: 'ts-connection-info-cell',
  templateUrl: './connection-info-cell.component.html',
  styleUrls: ['./connection-info-cell.component.scss']
})
export class ConnectionInfoCellComponent implements AgRendererComponent {
  values: any;

  constructor() {
  }

  agInit(params: any): void {
    if (params && params.value) {
      this.values = params.value;
    }
  }

  refresh(params: any): boolean {
    return false;
  }
}

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { configureTestSuite } from 'ng-bullet';

import { ConnectionInfoCellComponent } from './connection-info-cell.component';

describe('ConnectionInfoCellComponent', () => {
  let component: ConnectionInfoCellComponent;
  let fixture: ComponentFixture<ConnectionInfoCellComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ConnectionInfoCellComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionInfoCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

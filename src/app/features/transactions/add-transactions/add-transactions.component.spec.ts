import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { configureTestSuite } from 'ng-bullet';
import { provideMockStore } from '@ngrx/store/testing';

import { TranslateTestingModule } from '~app/test-utils';
import { AddTransactionsComponent } from './add-transactions.component';

describe('AddTransactionsComponent', () => {
  let component: AddTransactionsComponent;
  let fixture: ComponentFixture<AddTransactionsComponent>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule],
      declarations: [AddTransactionsComponent],
      providers: [
        provideMockStore({})
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

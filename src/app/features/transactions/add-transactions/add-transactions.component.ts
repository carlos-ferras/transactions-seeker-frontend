import { Component, ViewChild } from '@angular/core';

import { Store } from '@ngrx/store';

import { Transaction } from '~core/models';
import { AppSettingsActions, RootStore, TransactionsActions } from '~core/store';

@Component({
  selector: 'ts-add-transactions',
  templateUrl: './add-transactions.component.html',
  styleUrls: ['./add-transactions.component.scss']
})
export class AddTransactionsComponent {
  @ViewChild('fileInput')
  fileInput;
  allowedFileTypes = [
    'application/json'
  ];

  constructor(private store: Store<RootStore.State>) {
  }

  uploadGraph() {
    this.fileInput.nativeElement.click();
  }

  fileChangeListener(event: any): void {
    const file: File = event.target.files.length > 0 ? event.target.files[0] : null;
    if (file && this.allowedFileTypes.includes(file.type)) {
      this.loadTransactionsFromFile(file);
    } else if (file) {
      this.store.dispatch(
        new AppSettingsActions.PushErrorAction({
          error: 'The file you try to upload is not a JSON file.'
        })
      );
    }
  }

  loadTransactionsFromFile(file: File) {
    const reader = new FileReader();
    reader.onload = (event: any) => {
      let transactions;
      try {
        transactions = JSON.parse(event.target.result);
      } catch (err) {
        this.store.dispatch(
          new AppSettingsActions.PushErrorAction({
            error: 'The file you try to upload doesn\'t contain a valid JSON.'
          })
        );
        return;
      }
      this.saveTransactions(transactions);
    };
    reader.readAsText(file);
  }

  saveTransactions(transactions: Transaction[]) {
    if (Array.isArray(transactions)) {
      transactions.forEach((transaction) => this.saveTransaction(transaction));
    } else if (transactions && typeof transactions === 'object') {
      this.saveTransaction(transactions);
    }
  }

  saveTransaction(transaction: Transaction) {
    this.store.dispatch(
      new TransactionsActions.AddTransactionAction({transaction})
    );
  }
}

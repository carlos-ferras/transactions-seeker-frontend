import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { _ } from '@biesbjerg/ngx-translate-extract/dist/utils/utils';

import { TransactionsComponent } from './transactions.component';

const routes: Routes = [
  {
    path: '',
    component: TransactionsComponent,
    data: {
      title: _('Transactions')
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionsRoutingModule {}

import { NgModule } from '@angular/core';

import { CoreModule } from '~app/core';
import { GridComponent } from './grid';
import { FiltersComponent } from './filters';
import { AddTransactionsComponent } from './add-transactions';
import { TransactionsComponent } from './transactions.component';
import { TransactionsRoutingModule } from './transactions-routing.module';
import { ConnectionInfoCellComponent } from './connection-info-cell';

@NgModule({
  imports: [CoreModule, TransactionsRoutingModule],
  declarations: [
    TransactionsComponent,
    FiltersComponent,
    GridComponent,
    ConnectionInfoCellComponent,
    AddTransactionsComponent
  ],
  entryComponents: [
    ConnectionInfoCellComponent
  ]
})
export class TransactionsModule {
}

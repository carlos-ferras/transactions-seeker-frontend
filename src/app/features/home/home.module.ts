import { NgModule } from '@angular/core';

import { CoreModule } from '~app/core';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { NavbarComponent } from './navbar';

@NgModule({
  imports: [CoreModule, HomeRoutingModule],
  declarations: [HomeComponent, NavbarComponent]
})
export class HomeModule {
}

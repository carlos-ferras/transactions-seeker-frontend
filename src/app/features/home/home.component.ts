import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';

import { Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { takeUntil } from 'rxjs/operators';
import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';

import { RootStore, RoutingSelectors } from '~core/store';

@Component({
  selector: 'ts-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit, OnDestroy {
  @ViewChild(PerfectScrollbarComponent)
  scrollCtrl: PerfectScrollbarComponent;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private store: Store<RootStore.State>) {
  }

  ngAfterViewInit() {
    this.store.select(RoutingSelectors.selectCurrentRoute)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        this.scrollCtrl.directiveRef.scrollToTop(0, 0);
      });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

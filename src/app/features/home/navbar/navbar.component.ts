import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { takeUntil } from 'rxjs/operators';

import { AppSettingsActions, AppSettingsSelectors, RootStore } from '~core/store';

@Component({
  selector: 'ts-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
  langs: {
    code: string;
    name: string;
  }[];
  currentLang: string;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private store: Store<RootStore.State>) {
  }

  ngOnInit(): void {
    this.store.select(AppSettingsSelectors.selectAvailableLangs)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((langs: any[]) => {
        this.langs = langs;
      });

    this.store.select(AppSettingsSelectors.selectCurrentLang)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((lang: string) => {
        this.currentLang = lang;
      });
  }

  setLang(lang: string) {
    this.store.dispatch(
      new AppSettingsActions.SetCurrentLangAction({lang})
    );
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

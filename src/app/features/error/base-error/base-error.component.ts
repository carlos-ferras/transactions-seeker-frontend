import { Component, Input, OnDestroy, OnInit } from '@angular/core';

import { Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { takeUntil } from 'rxjs/operators';

import { RootStore, RoutingActions, RoutingSelectors } from '~core/store';
import { Route } from '~core/models';

@Component({
  selector: 'ts-base-error',
  templateUrl: './base-error.component.html'
})
export class BaseErrorComponent implements OnInit, OnDestroy {
  @Input()
  errorNumber: string;
  @Input()
  errorName: string;
  @Input()
  errorDescription: string;
  hasReload: boolean;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private store: Store<RootStore.State>) {
  }

  ngOnInit(): void {
    this.store.select(RoutingSelectors.selectCurrentRoute)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((route: Route) => {
        this.hasReload = !route.urlAfterRedirects.startsWith('/error');
      });
  }

  reload() {
    this.store.dispatch(
      new RoutingActions.RoutingGoForwardAction()
    );
  }

  goBack() {
    this.store.dispatch(
      new RoutingActions.RoutingGoBackAction()
    );
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

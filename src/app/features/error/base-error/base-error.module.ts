import { NgModule } from '@angular/core';

import { CoreModule } from '~app/core';
import { BaseErrorComponent } from './base-error.component';

@NgModule({
  imports: [CoreModule],
  declarations: [BaseErrorComponent],
  exports: [BaseErrorComponent]
})
export class BaseErrorModule {}

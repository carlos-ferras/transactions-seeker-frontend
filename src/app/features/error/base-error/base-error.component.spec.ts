import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { Store } from '@ngrx/store';
import { configureTestSuite } from 'ng-bullet';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { ActionTypes } from '~core/store/routing/routing.actions';
import { TranslateTestingModule } from '~app/test-utils';
import { BaseErrorComponent } from './base-error.component';

const STORE_STATE = {
  routing: {
    currentRoute: {
      urlAfterRedirects: '/'
    }
  }
};

describe('BaseErrorComponent', () => {
  let component: BaseErrorComponent;
  let fixture: ComponentFixture<BaseErrorComponent>;
  let store: MockStore<any>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule],
      declarations: [BaseErrorComponent],
      providers: [
        provideMockStore({
          initialState: STORE_STATE
        })
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseErrorComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.hasReload).toBeTruthy();
  });

  it('should update reload indicator to false if is an error page', () => {
    STORE_STATE.routing.currentRoute.urlAfterRedirects = '/error/404';
    store.setState(STORE_STATE);
    expect(component.hasReload).toBeFalsy();
  });

  it('should dispatch go forward to reload the page', () => {
    const spy = jest.spyOn(store, 'dispatch');
    component.reload();
    expect(spy.mock.calls[0][0].type).toEqual(ActionTypes.ROUTING_GO_FORWARD);
  });

  it('should dispatch go back', () => {
    const spy = jest.spyOn(store, 'dispatch');
    component.goBack();
    expect(spy.mock.calls[0][0].type).toEqual(ActionTypes.ROUTING_GO_BACK);
  });
});

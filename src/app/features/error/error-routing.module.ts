import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ErrorsComponent } from './errors.component';

const routes: Routes = [
  {
    path: '',
    component: ErrorsComponent,
    runGuardsAndResolvers: 'always',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '404'
      },
      {
        path: '401',
        loadChildren: './401/error-401.module#Error401Module'
      },
      {
        path: '403',
        loadChildren: './403/error-403.module#Error403Module'
      },
      {
        path: '404',
        loadChildren: './404/error-404.module#Error404Module'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorRoutingModule {}

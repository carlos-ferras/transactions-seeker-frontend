import { NgModule } from '@angular/core';

import { CoreModule } from '~app/core';
import { BaseErrorModule } from '../base-error';
import { Error404RoutingModule } from './error-404-routing.module';
import { PageNotFoundComponent } from './404.component';

@NgModule({
  imports: [CoreModule, Error404RoutingModule, BaseErrorModule],
  declarations: [PageNotFoundComponent]
})
export class Error404Module {}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { _ } from '@biesbjerg/ngx-translate-extract/dist/utils/utils';

import { PageNotFoundComponent } from './404.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: PageNotFoundComponent,
    data: {
      title: _('Page not found')
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Error404RoutingModule {}

import { Component } from '@angular/core';

@Component({
  selector: 'ts-error-403',
  templateUrl: './403.component.html'
})
export class ForbiddenComponent {}

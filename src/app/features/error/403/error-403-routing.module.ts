import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { _ } from '@biesbjerg/ngx-translate-extract/dist/utils/utils';

import { ForbiddenComponent } from './403.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ForbiddenComponent,
    data: {
      title: _('Forbidden')
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Error403RoutingModule {}

import { NgModule } from '@angular/core';

import { CoreModule } from '~app/core';
import { BaseErrorModule } from '../base-error';
import { Error403RoutingModule } from './error-403-routing.module';
import { ForbiddenComponent } from './403.component';

@NgModule({
  imports: [CoreModule, Error403RoutingModule, BaseErrorModule],
  declarations: [ForbiddenComponent]
})
export class Error403Module {}

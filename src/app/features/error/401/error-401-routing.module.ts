import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { _ } from '@biesbjerg/ngx-translate-extract/dist/utils/utils';

import { UnauthorizedComponent } from './401.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: UnauthorizedComponent,
    data: {
      title: _('Unauthorized')
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Error401RoutingModule {}

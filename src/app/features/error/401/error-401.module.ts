import { NgModule } from '@angular/core';

import { CoreModule } from '~app/core';
import { BaseErrorModule } from '../base-error';
import { Error401RoutingModule } from './error-401-routing.module';
import { UnauthorizedComponent } from './401.component';

@NgModule({
  imports: [CoreModule, Error401RoutingModule, BaseErrorModule],
  declarations: [UnauthorizedComponent]
})
export class Error401Module {}

import { Component } from '@angular/core';

@Component({
  selector: 'ts-error-401',
  templateUrl: './401.component.html'
})
export class UnauthorizedComponent {}

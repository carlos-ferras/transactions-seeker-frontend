import { NgModule } from '@angular/core';

import { CoreModule } from '~app/core';
import { ErrorRoutingModule } from './error-routing.module';
import { ErrorsComponent } from './errors.component';

@NgModule({
  imports: [CoreModule, ErrorRoutingModule],
  declarations: [ErrorsComponent]
})
export class ErrorModule {}

import _ from 'lodash';

export const translationsHandler = {
  get(target, name) {
    if (target.hasOwnProperty(name)) {
      return target[name];
    }
    return name;
  }
};

export const getTranslationsProxy = (translations: any): any => {
  const translationsProxy = new Proxy({}, translationsHandler);
  _.forEach(translations, (value: string | Function, key: string) => {
    if (typeof value !== 'string') {
      try {
        value = value();
      } catch (e) {
        value = key;
      }
    }
    translationsProxy[key] = value;
  });
  return translationsProxy;
};

import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { split } from 'apollo-link';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLink, HttpLinkModule } from 'apollo-angular-link-http';

import { environment } from '~env/environment';

export function createApollo(httpClient: HttpClient) {
  const httpLink = new HttpLink(httpClient).create({
    uri: `${window.location.origin}${environment['tsApiServer']}`
  });

  const subscriptionLink = new WebSocketLink({
    uri: `ws://${window.location.host}${environment['tsApiSubscriptions']}`,
    options: {
      reconnect: true
    }
  });

  const link = split(
    ({ query }) => {
      const { kind, operation } = getMainDefinition(query);
      return kind === 'OperationDefinition' && operation === 'subscription';
    },
    subscriptionLink,
    httpLink
  );

  return {
    link,
    cache: new InMemoryCache()
  };
}

@NgModule({
  exports: [ApolloModule, HttpLinkModule],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpClient]
    }
  ]
})
export class GraphQLModule {
}

import { NgModule } from '@angular/core';
import {
  DateAdapter,
  MatAutocompleteModule,
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatProgressBarModule,
  MatSnackBarModule,
  MatToolbarModule,
  MatTooltipModule,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  MAT_DIALOG_DEFAULT_OPTIONS
} from '@angular/material';
import {
  MatMomentDateModule,
  MomentDateAdapter,
  MAT_MOMENT_DATE_FORMATS
} from '@angular/material-moment-adapter';

const CUSTOM_MAT_DIALOG_DEFAULT_OPTIONS = {
  closeOnNavigation: true,
  autoFocus: true,
  hasBackdrop: true,
  disableClose: false
};

@NgModule({
  imports: [
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatDialogModule,
    MatInputModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatMenuModule,
    MatTooltipModule,
    MatToolbarModule,
    MatAutocompleteModule,
    MatMomentDateModule
  ],
  exports: [
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatMenuModule,
    MatDialogModule,
    MatInputModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatAutocompleteModule,
    MatMomentDateModule
  ],
  providers: [
    {
      provide: MAT_DATE_LOCALE,
      useValue: 'en'
    },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: MAT_MOMENT_DATE_FORMATS
    },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: CUSTOM_MAT_DIALOG_DEFAULT_OPTIONS
    }
  ]
})
export class MaterialModule {
}

import { ModuleWithProviders, NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { SharedModule } from '~app/shared';
import { ErrorsInterceptor } from '~core/interceptors';
import { GraphqlService, SeoService, SplashScreenService, TransactionsService } from './services';

@NgModule({
  imports: [
    HttpClientModule,
    RouterModule,
    SharedModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorsInterceptor,
      multi: true
    },
  ],
  exports: [
    SharedModule,
    RouterModule,
    HttpClientModule
  ]
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        SplashScreenService,
        SeoService,
        GraphqlService,
        TransactionsService
      ]
    };
  }
}

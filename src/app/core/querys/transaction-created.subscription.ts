import gql from 'graphql-tag';

import { TRANSACTION_FIELDS_FRAGMENT } from './transaction-fields.fragment';
import { TRANSACTIONS_RECURSIVE_FRAGMENT } from './transactions-recursive.fragment';
import { TRANSACTION_CHILD_FIELDS_FRAGMENT } from './transaction-child-fields.fragment';

export const TRANSACTION_CREATED = gql`
  ${TRANSACTION_FIELDS_FRAGMENT}  
  
  ${TRANSACTION_CHILD_FIELDS_FRAGMENT}
  
  ${TRANSACTIONS_RECURSIVE_FRAGMENT}
  
  subscription TransactionCreated {
    transactionCreated {
      ...transactionRecursive
    }
  }
  `;

function getInDeep(deep = 5) {
  switch (deep) {
    case 0:
      return `childrens {
        ...transactionChildFields
      }`;
    default:
      return `childrens {
        ...transactionChildFields,
        ${getInDeep(deep - 1)}
      }`;

  }
}

export const TRANSACTIONS_RECURSIVE_FRAGMENT = `
  fragment transactionRecursive on Transaction {
    ...transactionFields,
    ${getInDeep(50)}
  }
  `;


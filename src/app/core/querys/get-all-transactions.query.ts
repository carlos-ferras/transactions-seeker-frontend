import gql from 'graphql-tag';

import { TRANSACTION_FIELDS_FRAGMENT } from './transaction-fields.fragment';
import { TRANSACTIONS_RECURSIVE_FRAGMENT } from './transactions-recursive.fragment';
import { TRANSACTION_CHILD_FIELDS_FRAGMENT } from './transaction-child-fields.fragment';

export const GET_ALL_TRANSACTIONS_QUERY = gql`
  ${TRANSACTION_FIELDS_FRAGMENT}  
  
  ${TRANSACTION_CHILD_FIELDS_FRAGMENT}
  
  ${TRANSACTIONS_RECURSIVE_FRAGMENT}

  query getAllTransactions {
    transactions {
      ...transactionRecursive
    }
  }
  `;

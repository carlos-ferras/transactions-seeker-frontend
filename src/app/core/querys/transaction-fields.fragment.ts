export const TRANSACTION_FIELDS_FRAGMENT = `
  fragment transactionFields on Transaction {
    id,
    name,
    email,
    phone,
    age,
    geoInfo {
      latitude, 
      longitude
    },
  }
  `;

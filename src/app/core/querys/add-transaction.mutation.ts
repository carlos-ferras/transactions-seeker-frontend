import gql from 'graphql-tag';

export const ADD_TRANSACTION_MUTATION = gql`
  mutation AddTransaction($transaction: TransactionInput!) {
    addTransaction(input: $transaction) {
      id
    }
  }
  `;

export const TRANSACTION_CHILD_FIELDS_FRAGMENT = `
  fragment transactionChildFields on Transaction {
   ...transactionFields,
    connectionInfo {
      type,
      confidence
    }
  }
  `;

export interface Route {
  id: number;
  url: string;
  urlAfterRedirects: string;
}

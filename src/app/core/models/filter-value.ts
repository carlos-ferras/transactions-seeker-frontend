export interface FilterValue {
  transactionId: string;
  confidenceLevel: number;
}

import {GeoInfo} from './geo-info';
import { ConnectionInfo } from './connection-info';

export interface Transaction {
  id: string;
  name: string;
  email: string;
  phone: string;
  age: number;
  geoInfo: GeoInfo;
  connectionInfo?: ConnectionInfo;
  childrens?: Transaction[];
}

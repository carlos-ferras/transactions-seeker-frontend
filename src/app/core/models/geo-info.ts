export interface GeoInfo {
  latitude: number;
  longitude: number;
}

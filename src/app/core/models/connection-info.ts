export interface ConnectionInfo {
  type: string | string[];
  confidence: number;
}

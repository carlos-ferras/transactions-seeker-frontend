import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRouteSnapshot } from '@angular/router';

import _ from 'lodash';

/**
 * This service will handle the route data to fill page title,
 * and meta tags.
 *
 * For it, the route should have defined:
 * ```
 *  data: {
 *    title: '<title_here>',
 *    metaTags: {
 *      description: '<description_meta_tag_content_here>',
 *      keywords: '<keywords_meta_tag_content_here>'
 *    }
 *  }
 * ```
 */
@Injectable()
export class SeoService {
  appName = 'Transactions Seeker';

  constructor(
    private titleService: Title,
    private metaService: Meta
  ) {
  }

  extractRouteData(root: ActivatedRouteSnapshot) {
    while (root) {
      if (root.children && root.children.length) {
        root = root.children[0];
      } else if (!_.isEmpty(root.data)) {
        this.setTitle(root.data['title']);
        this.setMetaTags(root.data['metaTags']);
        return;
      } else {
        this.setTitle(null);
        return;
      }
    }
  }

  setTitle(title: string) {
    if (title) {
      this.titleService.setTitle(`${this.appName} | ${title}`);
    } else {
      this.titleService.setTitle(this.appName);
    }
  }

  setMetaTags(metaTags: { [name: string]: string }) {
    if (metaTags) {
      _.forEach(metaTags, (value: string, key: string) => {
        this.metaService.addTag({
          name: key,
          content: value
        });
      });
    }
  }
}

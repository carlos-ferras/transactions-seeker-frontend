import { TestBed } from '@angular/core/testing';

import { of } from 'rxjs';
import { configureTestSuite } from 'ng-bullet';

import { provideMagicalMock, Mock } from '~app/test-utils';
import {
  ADD_TRANSACTION_MUTATION,
  GET_ALL_TRANSACTIONS_QUERY,
  TRANSACTION_CREATED,
  TRANSACTION_DELETED,
  TRANSACTION_UPDATED
} from '../querys';
import { GraphqlService } from './graphql.service';
import { TransactionsService } from './transactions.service';

const transaction = {
  id: 'test',
  age: 5,
  email: 'test@email',
  name: 'test',
  phone: '555',
  geoInfo: {
    latitude: 5,
    longitude: 5
  }
};

describe('TransactionsService', () => {
  let service: TransactionsService;
  let graphqlService: Mock<GraphqlService>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [
        TransactionsService,
        provideMagicalMock(GraphqlService)
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    service = TestBed.get(TransactionsService);
    graphqlService = TestBed.get(GraphqlService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should call GraphQL watchQuery when list', () => {
    graphqlService.watchQuery.mockReturnValue(
      of({ transactions: [] })
    );
    service.list();

    expect(graphqlService.watchQuery).toHaveBeenCalledWith(
      'transactions',
      GET_ALL_TRANSACTIONS_QUERY
    );
  });

  it('should call GraphQL mutate when add', () => {
    graphqlService.mutate.mockReturnValue(
      of({ transaction })
    );
    service.add(transaction);
    expect(graphqlService.mutate).toHaveBeenCalledWith(
      'transaction',
      ADD_TRANSACTION_MUTATION,
      { transaction }
    );
  });

  it('should call GraphQL subscribe when watchTransactionCreated', () => {
    graphqlService.subscribe.mockReturnValue(
      of({ transactionCreated: transaction })
    );
    service.watchTransactionCreated();

    expect(graphqlService.subscribe).toHaveBeenCalledWith(
      'transactionCreated',
      TRANSACTION_CREATED
    );
  });

  it('should call GraphQL subscribe when watchTransactionUpdated', () => {
    graphqlService.subscribe.mockReturnValue(
      of({ transactionUpdated: transaction })
    );
    service.watchTransactionUpdated();

    expect(graphqlService.subscribe).toHaveBeenCalledWith(
      'transactionUpdated',
      TRANSACTION_UPDATED
    );
  });

  it('should call GraphQL subscribe when watchTransactionDeleted', () => {
    graphqlService.subscribe.mockReturnValue(
      of({ transactionDeleted: transaction })
    );
    service.watchTransactionDeleted();

    expect(graphqlService.subscribe).toHaveBeenCalledWith(
      'transactionDeleted',
      TRANSACTION_DELETED
    );
  });
});

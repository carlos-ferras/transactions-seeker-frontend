import { async, TestBed } from '@angular/core/testing';

import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';
import { configureTestSuite } from 'ng-bullet';
import { of } from 'rxjs/internal/observable/of';
import { provideMockStore } from '@ngrx/store/testing';
import { ApolloTestingController, ApolloTestingModule } from 'apollo-angular/testing';

import { GraphqlService } from './graphql.service';

describe('GraphqlService', () => {
  let service: GraphqlService;
  let apollo: Apollo;

  configureTestSuite(() => {
    return TestBed.configureTestingModule({
      imports: [ApolloTestingModule],
      providers: [
        GraphqlService,
        provideMockStore({})
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    service = TestBed.get(GraphqlService);
    apollo = TestBed.get(Apollo);
  });

  afterEach(() => {
    TestBed.get(ApolloTestingController).verify();
  });

  it('should create', () => {
    return expect(service).toBeTruthy();
  });

  it('should execute the query and return response.data', async(() => {
    const query: any = {
      valueChanges: of({
        data: {
          test: 'apollo'
        }
      })
    };

    spyOn(apollo, 'watchQuery').and.returnValue(query);

    service
      .watchQuery(
        'test',
        gql`
          query {
            testQuery {
              id
            }
          }
        `
      )
      .subscribe((result) => {
        expect(apollo.watchQuery).toHaveBeenCalled();
        expect(result).toBe('apollo');
      });
  }));

  it('should execute the mutation and return the response', async(() => {
    spyOn(apollo, 'mutate').and.returnValue(of({
      data: {
        test: 'apollo'
      }
    }));

    service
      .mutate(
        'test',
        gql`
          mutation {
            testMutation {
              id
            }
          }
        `
      )
      .subscribe((result) => {
        expect(apollo.mutate).toHaveBeenCalled();
        expect(result).toBe('apollo');
      });
  }));

  it('should execute the subscription and return the response', async(() => {
    spyOn(apollo, 'subscribe').and.returnValue(of({
      data: {
        test: 'apollo'
      }
    }));

    service
      .subscribe(
        'test',
        gql`
          subscription {
            testSubscription {
              id
            }
          }
        `
      )
      .subscribe((result) => {
        expect(apollo.subscribe).toHaveBeenCalled();
        expect(result).toBe('apollo');
      });
  }));
});

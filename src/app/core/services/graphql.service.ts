import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { Store } from '@ngrx/store';
import { of, Observable } from 'rxjs';
import { DocumentNode } from 'graphql';
import { Apollo } from 'apollo-angular';
import { catchError, map } from 'rxjs/operators';

import { State } from '../store/state';
import { PushErrorAction } from '../store/app-settings/app-settings.actions';

@Injectable()
export class GraphqlService {
  constructor(
    private apollo: Apollo,
    private store: Store<State>
  ) {
  }

  mutate(mainAttrName: string, mutation: DocumentNode, variables = {}): Observable<any> {
    return this.apollo
      .mutate({
        mutation,
        variables
      }).pipe(
        map((result) => result.data[mainAttrName]),
        catchError((error: HttpErrorResponse) => {
          this.store.dispatch(
            new PushErrorAction({error: error.message})
          );
          return of(null)
        })
      );
  }

  watchQuery(mainAttrName: string, query: DocumentNode, variables = {}): Observable<any> {
    return this.apollo
      .watchQuery({
        query,
        variables,
        fetchPolicy: 'network-only'
      }).valueChanges.pipe(
        map((result) => result.data[mainAttrName]),
        catchError((error: HttpErrorResponse) => {
          this.store.dispatch(
            new PushErrorAction({error: error.message})
          );
          return of(null)
        })
      );
  }

  subscribe(mainAttrName: string, query: DocumentNode, variables = {}): Observable<any> {
    return this.apollo.subscribe({
      query,
      variables,
      fetchPolicy: 'network-only'
    }).pipe(
      map((result) => result.data[mainAttrName]),
      catchError((error: HttpErrorResponse) => {
        this.store.dispatch(
          new PushErrorAction({error: error.message})
        );
        return of(null)
      })
    );
  }
}

import { async, TestBed } from '@angular/core/testing';
import { Component, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';

import { configureTestSuite } from 'ng-bullet';

import { provideMagicalMock, Mock } from '~app/test-utils';
import { SeoService } from './seo.service';

@Component({
  selector: 'ts-test-component',
  template: ''
})
class StubComponent {
}

describe('SeoService', () => {
  let service: SeoService;
  let ngZone: NgZone;
  let router: Router;
  let route: ActivatedRoute;
  let titleService: Mock<Title>;
  let metaService: Mock<Meta>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {
            path: 'home',
            component: StubComponent,
            data: {
              title: 'test',
              metaTags: {
                description: 'test description',
                keywords: 'test, keywords'
              }
            }
          },
          {
            path: 'no_title',
            component: StubComponent,
            data: {
              metaTags: {
                description: 'test description',
                keywords: 'test, keywords'
              }
            }
          },
          {
            path: 'empty',
            component: StubComponent
          }
        ])
      ],
      declarations: [StubComponent],
      providers: [SeoService, provideMagicalMock(Title), provideMagicalMock(Meta)]
    }).compileComponents();
  });

  beforeEach(() => {
    service = TestBed.get(SeoService);
    ngZone = TestBed.get(NgZone);
    titleService = TestBed.get(Title);
    metaService = TestBed.get(Meta);
    router = TestBed.get(Router);
    route = TestBed.get(ActivatedRoute);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should set title, and meta tags', async(() => {
    ngZone.run(() => {
      router.navigate(['/home']).then(() => {
        service.extractRouteData(route.snapshot);
        expect(titleService.setTitle).toHaveBeenCalledWith(`${service.appName} | test`);
        expect(metaService.addTag.mock.calls.length).toBe(2);
      });
    });
  }));

  it('should set default title and meta tags only', async(() => {
    ngZone.run(() => {
      router.navigate(['/no_title']).then(() => {
        service.extractRouteData(route.snapshot);
        expect(titleService.setTitle).toHaveBeenCalledWith(service.appName);
        expect(metaService.addTag.mock.calls.length).toBe(2);
      });
    });
  }));

  it('should set only default title', async(() => {
    ngZone.run(() => {
      router.navigate(['/empty']).then(() => {
        service.extractRouteData(route.snapshot);
        expect(titleService.setTitle).toHaveBeenCalledWith(service.appName);
        expect(metaService.addTag.mock.calls.length).toBe(0);
      });
    });
  }));
});

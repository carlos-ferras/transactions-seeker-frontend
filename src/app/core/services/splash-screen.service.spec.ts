import { TestBed } from '@angular/core/testing';
import { DOCUMENT } from '@angular/common';
import { AnimationBuilder } from '@angular/animations';

import { configureTestSuite } from 'ng-bullet';

import { SplashScreenService } from './splash-screen.service';

class AnimationBuilderStub {
  build = jest.fn().mockReturnValue({
    create: () => {
      return {
        play: () => {
        }
      };
    }
  });
}

const DOCUMENT_STUB = {
  body: {
    querySelector: jest.fn()
  }
};

describe('SplashScreenService', () => {
  let service: SplashScreenService;
  let animationBuilder: AnimationBuilderStub;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [
        SplashScreenService,
        {
          provide: AnimationBuilder,
          useClass: AnimationBuilderStub
        },
        {
          provide: DOCUMENT,
          useValue: DOCUMENT_STUB
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    service = TestBed.get(SplashScreenService);
    animationBuilder = TestBed.get(AnimationBuilder);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should play hide animation', () => {
    service.hide();
    expect(animationBuilder.build).toHaveBeenCalled();
  });
});

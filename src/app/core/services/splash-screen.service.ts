import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { animate, style, AnimationBuilder, AnimationPlayer } from '@angular/animations';

@Injectable()
export class SplashScreenService {
  splashScreenEl: Element;
  animationPlayer: AnimationPlayer;

  constructor(
    @Inject(DOCUMENT) private document: any,
    private animationBuilder: AnimationBuilder
  ) {
    this.splashScreenEl = this.document.body.querySelector('#ts-splash-screen');
  }

  hide() {
    this.animationPlayer = this.animationBuilder
      .build([
        style({ opacity: '1' }),
        animate(
          '200ms ease',
          style({
            opacity: '0',
            zIndex: '-10'
          })
        )
      ])
      .create(this.splashScreenEl);

    this.animationPlayer.play();
  }
}

import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Transaction } from '~core/models';
import {
  ADD_TRANSACTION_MUTATION,
  GET_ALL_TRANSACTIONS_QUERY,
  TRANSACTION_CREATED,
  TRANSACTION_DELETED,
  TRANSACTION_UPDATED
} from '~core/querys';
import { GraphqlService } from './graphql.service';

@Injectable()
export class TransactionsService {
  constructor(public graphqlService: GraphqlService) {
  }

  list(): Observable<Transaction[]> {
    return this.graphqlService.watchQuery(
      'transactions',
      GET_ALL_TRANSACTIONS_QUERY
    )
  }

  add(transaction: Transaction): Observable<Transaction> {
    return this.graphqlService.mutate(
      'transaction',
      ADD_TRANSACTION_MUTATION,
      { transaction }
    )
  }

  watchTransactionCreated(): Observable<Transaction> {
    return this.graphqlService.subscribe(
      'transactionCreated',
      TRANSACTION_CREATED
    )
  }

  watchTransactionUpdated(): Observable<Transaction> {
    return this.graphqlService.subscribe(
      'transactionUpdated',
      TRANSACTION_UPDATED
    )
  }

  watchTransactionDeleted(): Observable<Transaction> {
    return this.graphqlService.subscribe(
      'transactionDeleted',
      TRANSACTION_DELETED
    )
  }
}

import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';

import { Store } from '@ngrx/store';
import { catchError } from 'rxjs/operators';
import { of, throwError, Observable } from 'rxjs';

import { RoutingActions } from '~core/store';

@Injectable()
export class ErrorsInterceptor implements HttpInterceptor {
  store: Store<any>;

  constructor(private injector: Injector) {
    this.store = this.injector.get(Store);
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(catchError(err => {
        if ([401, 403].includes(err.status)) {
          this.store.dispatch(
            new RoutingActions.RoutingGoToAction({
              path: [`/error/${err.status}`]
            })
          );
          return of(null);
        } else {
          return throwError(err);
        }
      }));
  }
}

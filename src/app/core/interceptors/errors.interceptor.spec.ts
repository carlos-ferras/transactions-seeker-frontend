import { TestBed } from '@angular/core/testing';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { configureTestSuite } from 'ng-bullet';
import { provideMockStore } from '@ngrx/store/testing';

import { TranslateTestingModule } from '~app/test-utils';
import { ErrorsInterceptor } from './errors.interceptor';

describe('AuthenticationInterceptor', () => {
  let http: HttpClient;
  let mock: HttpTestingController;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateTestingModule],
      providers: [
        provideMockStore({}),
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ErrorsInterceptor,
          multi: true
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    http = TestBed.get(HttpClient);
    mock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    mock.verify();
  });

  it('should catch 401 error', () => {
    http
      .get('/api')
      .pipe(
        catchError((err) => {
          expect(err).toBeTruthy();
          return of(true);
        })
      )
      .subscribe((response) => {
        expect(response).toBeTruthy();
      });

    const request = mock.expectOne(
      (req) => req.url === '/api'
    );

    request.flush(
      { message: 'Test 401 error' },
      {
        status: 401,
        statusText: 'Forbidden'
      }
    );
  });
});

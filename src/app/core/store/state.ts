import { AppSettingsStore } from './app-settings';
import { RoutingStore } from './routing';
import { TransactionsStore } from './transactions';

export interface State {
  appSettings: AppSettingsStore.State;
  routing: RoutingStore.State;
  transactionsList: TransactionsStore.State;
}

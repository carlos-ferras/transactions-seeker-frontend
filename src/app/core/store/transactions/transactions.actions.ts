import { Action } from '@ngrx/store';

import { FilterValue, Transaction } from '../../models';

export enum ActionTypes {
  LOAD_TRANSACTIONS = '[Transactions] Load Transactions',
  SET_TRANSACTIONS = '[Transactions] Set Transactions',
  SET_TRANSACTION = '[Transactions] Set Transaction',
  SET_UPDATED_TRANSACTION = '[Transactions] Set Updated Transaction',
  SET_DELETED_TRANSACTION = '[Transactions] Set Deleted Transaction',
  ADD_TRANSACTION = '[Transactions] Add Transaction',
  FILTER_TRANSACTIONS = '[Transactions] Filter Transactions',
  APPLY_FILTER = '[Transactions] Apply Filter'
}

export class LoadTransactionsAction implements Action {
  readonly type = ActionTypes.LOAD_TRANSACTIONS;
}

export class SetTransactionsAction implements Action {
  readonly type = ActionTypes.SET_TRANSACTIONS;

  constructor(public payload: { transactions: Transaction[] }) {
  }
}

export class SetTransactionAction implements Action {
  readonly type = ActionTypes.SET_TRANSACTION;

  constructor(public payload: { transaction: Transaction }) {
  }
}

export class SetUpdatedTransactionAction implements Action {
  readonly type = ActionTypes.SET_UPDATED_TRANSACTION;

  constructor(public payload: { transaction: Transaction }) {
  }
}

export class SetDeletedTransactionAction implements Action {
  readonly type = ActionTypes.SET_DELETED_TRANSACTION;

  constructor(public payload: { transaction: Transaction }) {
  }
}

export class AddTransactionAction implements Action {
  readonly type = ActionTypes.ADD_TRANSACTION;

  constructor(public payload: { transaction: Transaction }) {
  }
}

export class FilterTransactionsAction implements Action {
  readonly type = ActionTypes.FILTER_TRANSACTIONS;

  constructor(public payload: { filterValue: FilterValue }) {
  }
}

export class ApplyFiltersAction implements Action {
  readonly type = ActionTypes.APPLY_FILTER;
}

export type Actions = LoadTransactionsAction | SetTransactionsAction | SetTransactionAction | SetUpdatedTransactionAction | SetDeletedTransactionAction | AddTransactionAction | ApplyFiltersAction | FilterTransactionsAction;

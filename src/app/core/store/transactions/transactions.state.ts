import { FilterValue, Transaction } from '../../models';

export interface State {
  transactions: Transaction[],
  filteredTransactions: Transaction[],
  filterValue: FilterValue
}

export const initialState: State = {
  transactions: [],
  filteredTransactions: [],
  filterValue: {
    transactionId: '',
    confidenceLevel: 1
  }
};

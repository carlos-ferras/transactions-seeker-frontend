import { Actions, ActionTypes } from './transactions.actions';
import { initialState, State } from './transactions.state';
import { flattenTransactions, getFilteredTransactions } from './transactions.helpers';

export function TransactionsReducer(state = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.FILTER_TRANSACTIONS:
      return {
        ...state,
        filterValue: action.payload.filterValue
      };
    case ActionTypes.SET_TRANSACTIONS:
      return {
        ...state,
        transactions: action.payload.transactions || state.transactions
      };
    case ActionTypes.SET_UPDATED_TRANSACTION:
      const index = state.transactions.findIndex(
        (transaction) => transaction.id === action.payload.transaction.id
      );
      if (index > -1) {
        state.transactions = [
          ...state.transactions
        ];
        state.transactions[index] = action.payload.transaction;
      }
      return {
        ...state
      };
    case ActionTypes.SET_DELETED_TRANSACTION:
      return {
        ...state,
        transactions: state.transactions.filter(
          (transaction) => transaction.id !== action.payload.transaction.id
        )
      };
    case ActionTypes.SET_TRANSACTION:
    case ActionTypes.ADD_TRANSACTION:
      return {
        ...state,
        transactions: [
          ...state.transactions,
          action.payload.transaction
        ]
      };
    case ActionTypes.APPLY_FILTER:
      return {
        ...state,
        filteredTransactions: state.filterValue.transactionId
          ? flattenTransactions(
            getFilteredTransactions(
              state.transactions,
              state.filterValue
            )
          )
          : []
      };
  }
  return state;
}

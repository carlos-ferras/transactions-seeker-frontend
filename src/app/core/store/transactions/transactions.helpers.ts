import { FilterValue, Transaction } from '~core/models';

function formatTransactionData(transaction: Transaction, connectionTrace: string[]): Transaction {
  const {
    childrens,
    ...formattedTransaction
  } = transaction;

  if (formattedTransaction.connectionInfo) {
    formattedTransaction.connectionInfo ={
      type: connectionTrace,
      confidence: transaction.connectionInfo.confidence
    }
  }

  return formattedTransaction;
}

function getUpdateTrace(trace: string[], addItem: string): string[] {
  return !addItem || trace.includes(addItem)
    ? trace
    : [...trace, addItem];
}

function filterChildrenByConfidence(transaction: Transaction, filterValue: FilterValue): Transaction {
  return {
    ...transaction,
    childrens: !transaction.childrens
      ? []
      : transaction.childrens
        .filter((childTransaction: Transaction) =>
          childTransaction.connectionInfo && childTransaction.connectionInfo.confidence >= (filterValue.confidenceLevel || 0)
        )
        .map((childTransaction: Transaction) =>
          filterChildrenByConfidence(childTransaction, filterValue))
  };
}

function addRootTransaction(transactions: Transaction[], transaction: Transaction, filterValue: FilterValue) {
  delete transaction.connectionInfo;
  transactions.push(
    filterChildrenByConfidence(transaction, filterValue)
  );
}

export function flattenTransactions(transactions: Transaction[], connectionTrace = []): Transaction[] {
  return transactions.reduce(
    (previous, current) => {
      connectionTrace = getUpdateTrace(
        connectionTrace,
        current.connectionInfo ? current.connectionInfo.type as string : ''
      );
      return [
        ...previous,
        formatTransactionData(current, connectionTrace),
        ...current.childrens
          ? flattenTransactions(
            current.childrens,
            connectionTrace
          )
          : []
      ];
    }, []);
}

export function getFilteredTransactions(transactions: Transaction[], filterValue: FilterValue): Transaction[] {
  let filteredTransactions = [];
  transactions.forEach((transaction: Transaction) => {
    if (transaction.id.includes(filterValue.transactionId.trim())) {
      addRootTransaction(
        filteredTransactions,
        {...transaction},
        filterValue
      );
    } else if (transaction.childrens && transaction.childrens.length > 0) {
      filteredTransactions = [
        ...filteredTransactions,
        ...getFilteredTransactions(transaction.childrens, filterValue)
      ];
    }
  });
  return filteredTransactions;
}

import { Injectable } from '@angular/core';

import { ofType, Actions, Effect } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';

import { Transaction } from '~core/models';
import { TransactionsService } from '~core/services';
import { ActionTypes, AddTransactionAction, ApplyFiltersAction, SetTransactionsAction } from './transactions.actions';

@Injectable()
export class TransactionsEffects {
  @Effect()
  loadTransactionsEffect = this.actions
    .pipe(
      ofType(ActionTypes.LOAD_TRANSACTIONS),
      mergeMap(() => this.transactionsService.list()),
      map((transactions: Transaction[]) => new SetTransactionsAction({ transactions }))
    );

  @Effect()
  setTransactionsEffect = this.actions
    .pipe(
      ofType(ActionTypes.SET_TRANSACTIONS),
      map(() => new ApplyFiltersAction())
    );

  @Effect()
  setTransactionEffect = this.actions
    .pipe(
      ofType(ActionTypes.SET_TRANSACTION),
      map(() => new ApplyFiltersAction())
    );

  @Effect()
  setUpdatedTransactionEffect = this.actions
    .pipe(
      ofType(ActionTypes.SET_UPDATED_TRANSACTION),
      map(() => new ApplyFiltersAction())
    );

  @Effect()
  setDeletedTransactionEffect = this.actions
    .pipe(
      ofType(ActionTypes.SET_DELETED_TRANSACTION),
      map(() => new ApplyFiltersAction())
    );

  @Effect()
  filterTransactionsEffect = this.actions
    .pipe(
      ofType(ActionTypes.FILTER_TRANSACTIONS),
      map(() => new ApplyFiltersAction())
    );

  @Effect()
  addTransactionEffect = this.actions
    .pipe(
      ofType(ActionTypes.ADD_TRANSACTION),
      map((action: AddTransactionAction) => action.payload),
      mergeMap(({ transaction }) => this.transactionsService.add(transaction)),
      map(() => new ApplyFiltersAction())
    );

  constructor(
    private actions: Actions,
    private transactionsService: TransactionsService
  ) {
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { TransactionsReducer } from './transactions.reducer';
import { TransactionsEffects } from './transactions.effects';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature('transactionsList', TransactionsReducer),
    EffectsModule.forFeature([TransactionsEffects])
  ],
  providers: [TransactionsEffects]
})
export class TransactionsModule { }

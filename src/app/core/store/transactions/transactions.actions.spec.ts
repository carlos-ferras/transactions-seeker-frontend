import { TestBed } from '@angular/core/testing';
import { CommonModule } from '@angular/common';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { configureTestSuite } from 'ng-bullet';

import { provideMagicalMock } from '~app/test-utils';
import { TransactionsService } from '~core/services';
import { TransactionsEffects } from './transactions.effects';
import {
  Actions,
  ActionTypes,
  AddTransactionAction,
  ApplyFiltersAction,
  FilterTransactionsAction,
  LoadTransactionsAction,
  SetDeletedTransactionAction,
  SetTransactionsAction,
  SetTransactionAction,
  SetUpdatedTransactionAction
} from './transactions.actions';
import { TransactionsReducer } from './transactions.reducer';
import { initialState } from './transactions.state';

const transaction = {
  id: 'test',
  age: 5,
  email: 'test@email',
  name: 'test',
  phone: '555',
  geoInfo: {
    latitude: 5,
    longitude: 5
  }
};

describe('TransactionsActions', () => {
  let effects: TransactionsEffects;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        StoreModule.forRoot(TransactionsReducer),
        EffectsModule.forRoot([TransactionsEffects])
      ],
      providers: [
        TransactionsEffects,
        provideMagicalMock(TransactionsService)
      ]
    });
  });

  beforeEach(() => {
    effects = TestBed.get(TransactionsEffects);
  });

  it('should get initial', () => {
    const state = TransactionsReducer(undefined, {} as Actions);
    expect(state).toEqual(initialState);
  });

  it('should keep the state on LOAD_TRANSACTIONS action', () => {
    const action = new LoadTransactionsAction();
    expect(action).toEqual({
      type: ActionTypes.LOAD_TRANSACTIONS
    });

    const state = TransactionsReducer(undefined, action);
    expect(state).toEqual({
      ...initialState
    });
  });

  it('should update state on SET_TRANSACTIONS action', () => {
    const payload = {
      transactions: []
    };

    const action = new SetTransactionsAction(payload);
    expect(action).toEqual({
      type: ActionTypes.SET_TRANSACTIONS,
      payload
    });

    const state = TransactionsReducer(undefined, action);
    expect(state).toEqual({
      ...initialState,
      transactions: payload.transactions
    });
  });

  it('should update state on SET_TRANSACTION action', () => {
    const payload = {
      transaction
    };

    const action = new SetTransactionAction(payload);
    expect(action).toEqual({
      type: ActionTypes.SET_TRANSACTION,
      payload
    });

    const state = TransactionsReducer(undefined, action);
    expect(state).toEqual({
      ...initialState,
      transactions: [
        ...initialState.transactions,
        transaction
      ]
    });
  });

  it('should update state on SET_UPDATED_TRANSACTION action', () => {
    initialState.transactions = [transaction];
    const payload = {
      transaction: {
        ...transaction,
        name: 'different'
      }
    };

    const action = new SetUpdatedTransactionAction(payload);
    expect(action).toEqual({
      type: ActionTypes.SET_UPDATED_TRANSACTION,
      payload
    });

    const state = TransactionsReducer(undefined, action);
    expect(state).toEqual({
      ...initialState,
      transactions: [{
        ...transaction,
        name: 'different'
      }]
    });
  });

  it('should update state on SET_DELETED_TRANSACTION action', () => {
    const payload = {
      transaction
    };
    initialState.transactions = [transaction];

    const action = new SetDeletedTransactionAction(payload);
    expect(action).toEqual({
      type: ActionTypes.SET_DELETED_TRANSACTION,
      payload
    });

    const state = TransactionsReducer(undefined, action);
    expect(state).toEqual({
      ...initialState,
      transactions: initialState.transactions.filter((trans) => trans.id !== transaction.id)
    });
  });

  it('should update state on ADD_TRANSACTION action', () => {
    const payload = {
      transaction
    };

    const action = new AddTransactionAction(payload);
    expect(action).toEqual({
      type: ActionTypes.ADD_TRANSACTION,
      payload
    });

    const state = TransactionsReducer(undefined, action);
    expect(state).toEqual({
      ...initialState,
      transactions: [
        ...initialState.transactions,
        payload.transaction
      ]
    });
  });

  it('should update state on FILTER_TRANSACTIONS action', () => {
    const payload = {
      filterValue: {
        transactionId: 'test',
        confidenceLevel: 0.5
      }
    };

    const action = new FilterTransactionsAction(payload);
    expect(action).toEqual({
      type: ActionTypes.FILTER_TRANSACTIONS,
      payload
    });

    const state = TransactionsReducer(undefined, action);
    expect(state).toEqual({
      ...initialState,
      filterValue: payload.filterValue
    });
  });

  it('should update filtered transactions with 3 results on APPLY_FILTER action', () => {
    const currentState = {
      transactions: [
        {
          'id': 'test1',
          'age': 38,
          'name': 'Sanchez Collier',
          'email': 'sanchezcollier@equicom.com',
          'phone': '(897) 421-3152',
          'geoInfo': {
            'latitude': -66.117512,
            'longitude': -50.147742
          },
          'childrens': [
            {
              'id': 'test4 match',
              'age': 40,
              'name': 'Love Hoffman',
              'email': 'sanchezcollier@equicom.com',
              'phone': '(856) 488-3734',
              'connectionInfo': {
                'type': 'sameEmail',
                'confidence': 0.3
              },
              'geoInfo': {
                'latitude': 5.393689,
                'longitude': 91.586263
              },
              'childrens': []
            }
          ]
        },
        {
          'id': 'test2',
          'age': 21,
          'name': 'Fernandez Navarro',
          'email': 'fernandeznavarro@equicom.com',
          'phone': '(862) 411-3092',
          'geoInfo': {
            'latitude': 4.463818,
            'longitude': -79.440754
          },
          'childrens': [
            {
              'id': 'test5',
              'age': 20,
              'name': 'Fernandez Navarro',
              'email': 'shaunamckee@equicom.com',
              'phone': '(888) 562-3854',
              'connectionInfo': {
                'type': 'sameName',
                'confidence': 1
              },
              'geoInfo': {
                'latitude': -86.013184,
                'longitude': -95.186326
              },
              'childrens': [
                {
                  'id': 'test6 match',
                  'age': 23,
                  'name': 'Fernandez Navarro',
                  'email': 'shaunamckee@equicom.com',
                  'phone': '(989) 525-2851',
                  'connectionInfo': {
                    'type': 'sameEmail',
                    'confidence': 0.7
                  },
                  'geoInfo': {
                    'latitude': 42.04286,
                    'longitude': 124.724138
                  },
                  'childrens': []
                }
              ]
            }
          ]
        },
        {
          'id': 'test3 match',
          'age': 23,
          'name': 'Nora Richardson',
          'email': 'norarichardson@equicom.com',
          'phone': '(909) 469-2462',
          'geoInfo': {
            'latitude': -22.341707,
            'longitude': 20.418298
          },
          'childrens': []
        }
      ],
      filteredTransactions: [],
      filterValue: {
        transactionId: 'match',
        confidenceLevel: 1
      }
    };

    const action = new ApplyFiltersAction();
    expect(action).toEqual({
      type: ActionTypes.APPLY_FILTER
    });

    const state = TransactionsReducer(currentState, action);
    expect(state).toEqual({
      ...currentState,
      filteredTransactions: [
        {
          'id': 'test4 match',
          'age': 40,
          'name': 'Love Hoffman',
          'email': 'sanchezcollier@equicom.com',
          'phone': '(856) 488-3734',
          'geoInfo': {
            'latitude': 5.393689,
            'longitude': 91.586263
          }
        },
        {
          'id': 'test6 match',
          'age': 23,
          'name': 'Fernandez Navarro',
          'email': 'shaunamckee@equicom.com',
          'phone': '(989) 525-2851',
          'geoInfo': {
            'latitude': 42.04286,
            'longitude': 124.724138
          }
        },
        {
          'id': 'test3 match',
          'age': 23,
          'name': 'Nora Richardson',
          'email': 'norarichardson@equicom.com',
          'phone': '(909) 469-2462',
          'geoInfo': {
            'latitude': -22.341707,
            'longitude': 20.418298
          }
        }
      ]
    });
  });

  it('should update filtered transactions by confidence level on APPLY_FILTER action', () => {
    const currentState = {
      transactions: [
        {
          'id': 'test1 match',
          'age': 21,
          'name': 'Fernandez Navarro',
          'email': 'fernandeznavarro@equicom.com',
          'phone': '(862) 411-3092',
          'geoInfo': {
            'latitude': 4.463818,
            'longitude': -79.440754
          },
          'childrens': [
            {
              'id': 'test2',
              'age': 20,
              'name': 'Fernandez Navarro',
              'email': 'shaunamckee@equicom.com',
              'phone': '(888) 562-3854',
              'connectionInfo': {
                'type': 'sameName',
                'confidence': 1
              },
              'geoInfo': {
                'latitude': -86.013184,
                'longitude': -95.186326
              },
              'childrens': [
                {
                  'id': 'test3',
                  'age': 23,
                  'name': 'Fernandez Navarro',
                  'email': 'shaunamckee@equicom.com',
                  'phone': '(989) 525-2851',
                  'connectionInfo': {
                    'type': 'sameEmail',
                    'confidence': 0.3
                  },
                  'geoInfo': {
                    'latitude': 42.04286,
                    'longitude': 124.724138
                  },
                  'childrens': []
                }
              ]
            }
          ]
        }
      ],
      filteredTransactions: [],
      filterValue: {
        transactionId: 'match',
        confidenceLevel: 0.5
      }
    };

    const action = new ApplyFiltersAction();
    expect(action).toEqual({
      type: ActionTypes.APPLY_FILTER
    });

    const state = TransactionsReducer(currentState, action);
    expect(state).toEqual({
      ...currentState,
      filteredTransactions: [
        {
          'id': 'test1 match',
          'age': 21,
          'name': 'Fernandez Navarro',
          'email': 'fernandeznavarro@equicom.com',
          'phone': '(862) 411-3092',
          'geoInfo': {
            'latitude': 4.463818,
            'longitude': -79.440754
          }
        },
        {
          'id': 'test2',
          'age': 20,
          'name': 'Fernandez Navarro',
          'email': 'shaunamckee@equicom.com',
          'phone': '(888) 562-3854',
          'connectionInfo': {
            'type': ['sameName'],
            'confidence': 1
          },
          'geoInfo': {
            'latitude': -86.013184,
            'longitude': -95.186326
          }
        }
      ]
    });
  });
});

import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';

import { FilterValue, Transaction } from '../../models';
import { initialState, State } from './transactions.state';

export const getTransactions = (state: State): Transaction[] => state ? state.transactions : initialState.transactions;
export const getFilteredTransactions= (state: State): Transaction[] => state ? state.filteredTransactions : initialState.filteredTransactions;
export const getFilterValue = (state: State): FilterValue => state ? state.filterValue : initialState.filterValue;

export const selectTransactionsState: MemoizedSelector<object, State> = createFeatureSelector<State>('transactionsList');

export const selectTransactions: MemoizedSelector<object, Transaction[]> = createSelector(
  selectTransactionsState,
  getTransactions
);

export const selectFilteredTransactions: MemoizedSelector<object, Transaction[]> = createSelector(
  selectTransactionsState,
  getFilteredTransactions
);

export const selectFilterValue: MemoizedSelector<object, FilterValue> = createSelector(
  selectTransactionsState,
  getFilterValue
);

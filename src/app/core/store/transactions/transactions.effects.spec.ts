import { TestBed } from '@angular/core/testing';
import { CommonModule } from '@angular/common';

import { Observable } from 'rxjs';
import { Actions } from '@ngrx/effects';
import { cold, hot } from 'jest-marbles';
import { StoreModule } from '@ngrx/store';
import { configureTestSuite } from 'ng-bullet';

import { provideMagicalMock, Mock } from '~app/test-utils';
import { TransactionsService } from '~core/services';
import { TransactionsEffects } from './transactions.effects';
import {
  AddTransactionAction,
  ApplyFiltersAction,
  FilterTransactionsAction,
  LoadTransactionsAction,
  SetDeletedTransactionAction,
  SetTransactionsAction,
  SetTransactionAction,
  SetUpdatedTransactionAction
} from './transactions.actions';
import { TransactionsReducer } from './transactions.reducer';


class TestActions extends Actions {
  constructor() {
    super();
  }

  set stream(source: Observable<any>) {
    this.source = source;
  }
}

function getActions() {
  return new TestActions();
}

const transaction = {
  id: 'test',
  age: 5,
  email: 'test@email',
  name: 'test',
  phone: '555',
  geoInfo: {
    latitude: 5,
    longitude: 5
  }
};

describe('TransactionsEffects', () => {
  let actions: TestActions;
  let effects: TransactionsEffects;
  let transactionsService: Mock<TransactionsService>;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        StoreModule.forFeature('routing', TransactionsReducer),
        StoreModule.forRoot({})
      ],
      providers: [
        TransactionsEffects,
        {
          provide: Actions,
          useFactory: getActions
        },
        provideMagicalMock(TransactionsService)
      ]
    });
  });

  beforeEach(() => {
    actions = TestBed.get(Actions);
    effects = TestBed.get(TransactionsEffects);
    transactionsService = TestBed.get(TransactionsService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should load transactions from API on LOAD_TRANSACTIONS action', () => {
    const payload = {
      transactions: []
    };
    transactionsService.list.mockReturnValue(payload.transactions);

    const action = new LoadTransactionsAction();
    const outcome = new SetTransactionsAction(payload);

    actions.stream = hot('--a', { a: action });
    const expected = cold('--b', { b: outcome });

    effects.loadTransactionsEffect.subscribe(result => {
      expect(result).toEqual(expected.values['b']);
      expect(transactionsService.list).toHaveBeenCalled();
    });
  });

  it('should apply filters after SET_TRANSACTIONS action', () => {
    const payload = {
      transactions: []
    };

    const action = new SetTransactionsAction(payload);
    const outcome = new ApplyFiltersAction();

    actions.stream = hot('--a', { a: action });
    const expected = cold('--b', { b: outcome });

    effects.setTransactionsEffect.subscribe(result => {
      expect(result).toEqual(expected.values['b']);
    });
  });

  it('should apply filters after SET_TRANSACTION action', () => {
    const payload = {
      transaction
    };

    const action = new SetTransactionAction(payload);
    const outcome = new ApplyFiltersAction();

    actions.stream = hot('--a', { a: action });
    const expected = cold('--b', { b: outcome });

    effects.setTransactionsEffect.subscribe(result => {
      expect(result).toEqual(expected.values['b']);
    });
  });

  it('should apply filters after SET_UPDATED_TRANSACTION action', () => {
    const payload = {
      transaction
    };

    const action = new SetUpdatedTransactionAction(payload);
    const outcome = new ApplyFiltersAction();

    actions.stream = hot('--a', { a: action });
    const expected = cold('--b', { b: outcome });

    effects.setTransactionsEffect.subscribe(result => {
      expect(result).toEqual(expected.values['b']);
    });
  });

  it('should apply filters after SET_DELETED_TRANSACTION action', () => {
    const payload = {
      transaction
    };

    const action = new SetDeletedTransactionAction(payload);
    const outcome = new ApplyFiltersAction();

    actions.stream = hot('--a', { a: action });
    const expected = cold('--b', { b: outcome });

    effects.setTransactionsEffect.subscribe(result => {
      expect(result).toEqual(expected.values['b']);
    });
  });

  it('should apply filters after ADD_TRANSACTION action', () => {
    const payload = {
      transaction
    };

    const action = new AddTransactionAction(payload);
    const outcome = new ApplyFiltersAction();

    actions.stream = hot('--a', { a: action });
    const expected = cold('--b', { b: outcome });

    effects.setTransactionsEffect.subscribe(result => {
      expect(result).toEqual(expected.values['b']);
    });
  });

  it('should load transactions from API on FILTER_TRANSACTIONS action', () => {
    const payload = {
      filterValue: {
        transactionId: 'test',
        confidenceLevel: 1
      }
    };

    const action = new FilterTransactionsAction(payload);
    const outcome = new ApplyFiltersAction();

    actions.stream = hot('--a', { a: action });
    const expected = cold('--b', { b: outcome });

    effects.filterTransactionsEffect.subscribe(result => {
      expect(result).toEqual(expected.values['b']);
    });
  });
});

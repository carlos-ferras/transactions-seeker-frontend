import { RouterTestingModule } from '@angular/router/testing';
import { TestBed } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';

import { Actions } from '@ngrx/effects';
import { cold } from 'jest-marbles';
import { Observable } from 'rxjs';
import { StoreModule } from '@ngrx/store';
import { configureTestSuite } from 'ng-bullet';
import { TranslateService } from '@ngx-translate/core';

import { provideMagicalMock } from '~app/test-utils';
import { RoutingEffects } from './routing.effects';
import { RoutingGoBackAction, RoutingGoForwardAction, RoutingGoToAction } from './routing.actions';
import { RoutingReducer } from './routing.reducer';


export class TestActions extends Actions {
  constructor() {
    super();
  }

  set stream(source: Observable<any>) {
    this.source = source;
  }
}

export function getActions() {
  return new TestActions();
}


describe('RoutingEffects', () => {
  let actions: TestActions;
  let effects: RoutingEffects;
  let translateService: TranslateService;
  let router: Router;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        RouterTestingModule,
        StoreModule.forFeature('routing', RoutingReducer),
        StoreModule.forRoot({})
      ],
      providers: [
        RoutingEffects,
        {
          provide: Actions,
          useFactory: getActions
        },
        provideMagicalMock(TranslateService)
      ]
    });
  });

  beforeEach(() => {
    translateService = TestBed.get(TranslateService);
    actions = TestBed.get(Actions);

    router = TestBed.get(Router);
    jest.spyOn(router, 'navigate').mockImplementation(() => Promise.resolve(true));
    jest.spyOn(router, 'navigateByUrl').mockImplementation(() => Promise.resolve(true));

    effects = TestBed.get(RoutingEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should navigate after ROUTING_GO_TO action', () => {
    const payload = {
      path: ['/home']
    };
    const action = new RoutingGoToAction(payload);
    actions.stream = cold('--a', { a: action });

    effects.navigate.subscribe(result => {
      expect(result).toEqual(payload);
      expect(router.navigate).toHaveBeenCalledWith(payload.path);
    });
  });

  it('should navigate after ROUTING_GO_BACK action', () => {
    const action = new RoutingGoBackAction();
    actions.stream = cold('--a', { a: action });

    effects.navigateBack.subscribe(() => {
      expect(router.navigateByUrl).toHaveBeenCalled();
    });
  });

  it('should navigate after ROUTING_GO_FORWARD action', () => {
    const action = new RoutingGoForwardAction();
    actions.stream = cold('--a', { a: action });

    effects.navigateForward.subscribe(() => {
      expect(router.navigateByUrl).toHaveBeenCalled();
    });
  });
});

import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';

import { Route } from '~core/models';
import { initialState, State } from './routing.state';

export const getCurrentRouteIndex = (state: State): number => state ? state.currentIndex : initialState.currentIndex;
export const getCurrentRoute = (state: State): Route => state ? state.currentRoute : initialState.currentRoute;
export const getRoutingHistory = (state: State): Route[] => state ? state.history : initialState.history;

export const selectRoutingState: MemoizedSelector<object, State> = createFeatureSelector<State>('routing');

export const selectCurrentRouteIndex: MemoizedSelector<object, number> = createSelector(
  selectRoutingState,
  getCurrentRouteIndex
);

export const selectCurrentRoute: MemoizedSelector<object, Route> = createSelector(
  selectRoutingState,
  getCurrentRoute
);

export const selectRoutingHistory: MemoizedSelector<object, Route[]> = createSelector(
  selectRoutingState,
  getRoutingHistory
);

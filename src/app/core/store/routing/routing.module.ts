import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { RoutingReducer } from './routing.reducer';
import { RoutingEffects } from './routing.effects';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature('routing', RoutingReducer),
    EffectsModule.forFeature([RoutingEffects])
  ],
  providers: [RoutingEffects]
})
export class RoutingModule { }

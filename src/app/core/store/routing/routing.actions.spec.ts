import { RouterTestingModule } from '@angular/router/testing';
import { TestBed } from '@angular/core/testing';
import { CommonModule } from '@angular/common';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { configureTestSuite } from 'ng-bullet';
import { TranslateService } from '@ngx-translate/core';

import { provideMagicalMock } from '~app/test-utils';
import { RoutingEffects } from './routing.effects';
import {
  Actions,
  ActionTypes,
  RoutingChangeAction,
  RoutingGoBackAction,
  RoutingGoForwardAction,
  RoutingGoToAction
} from './routing.actions';
import { RoutingReducer } from './routing.reducer';
import { initialState } from './routing.state';

describe('RoutingActions', () => {
  let effects: RoutingEffects;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        RouterTestingModule,
        StoreModule.forRoot(RoutingReducer),
        EffectsModule.forRoot([RoutingEffects])
      ],
      providers: [
        RoutingEffects,
        provideMagicalMock(TranslateService)
      ]
    });
  });

  beforeEach(() => {
    effects = TestBed.get(RoutingEffects);
  });

  it('should get initial', () => {
    const state = RoutingReducer(undefined, {} as Actions);
    expect(state).toEqual(initialState);
  });

  it('should keep the state on ROUTING_GO_TO action', () => {
    const payload = {
      path: ['/home']
    };

    const action = new RoutingGoToAction(payload);
    expect(action).toEqual({
      type: ActionTypes.ROUTING_GO_TO,
      payload
    });

    const state = RoutingReducer(undefined, action);
    expect(state).toEqual({
      ...initialState
    });
  });

  it('should update state on ROUTING_GO_FORWARD action', () => {
    const action = new RoutingGoForwardAction();
    expect(action).toEqual({
      type: ActionTypes.ROUTING_GO_FORWARD
    });

    const state = RoutingReducer(undefined, action);
    expect(state).toEqual({
      ...initialState,
      isFreeNavigation: false
    });
  });

  it('should update state on ROUTING_GO_BACK action', () => {
    const action = new RoutingGoBackAction();
    expect(action).toEqual({
      type: ActionTypes.ROUTING_GO_BACK,
    });

    const state = RoutingReducer(undefined, action);
    expect(state).toEqual({
      ...initialState,
      isFreeNavigation: false
    });
  });

  it('should update state on ROUTING_CHANGE action', () => {
    const payload = {
      id: 1,
      url: '/home',
      urlAfterRedirects: '/home'
    };

    const action = new RoutingChangeAction(payload);
    expect(action).toEqual({
      type: ActionTypes.ROUTING_CHANGE,
      payload
    });

    const state = RoutingReducer(undefined, action);
    expect(state).toEqual({
      ...initialState,
      currentRoute: payload,
      history: [
        ...initialState.history,
        payload
      ]
    });
  });
});

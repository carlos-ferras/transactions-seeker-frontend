import { NavigationExtras, Params } from '@angular/router';

import { Action } from '@ngrx/store';

import { Route } from '~core/models';

export enum ActionTypes {
  ROUTING_GO_TO = '[Routing] Go To',
  ROUTING_GO_BACK = '[Routing] Go Back',
  ROUTING_GO_FORWARD = '[Routing] Go Forward',
  ROUTING_CHANGE = '[Routing] Change',
}

export class RoutingGoToAction implements Action {
  readonly type = ActionTypes.ROUTING_GO_TO;

  constructor(
    public payload: {
      path: any[];
      queryParams?: Params;
      extras?: NavigationExtras;
    }
  ) {
  }
}

export class RoutingGoBackAction implements Action {
  readonly type = ActionTypes.ROUTING_GO_BACK;
}

export class RoutingGoForwardAction implements Action {
  readonly type = ActionTypes.ROUTING_GO_FORWARD;
}

export class RoutingChangeAction implements Action {
  readonly type = ActionTypes.ROUTING_CHANGE;

  constructor(public payload: Route) {
  }
}

export type Actions = RoutingGoToAction | RoutingGoBackAction | RoutingGoForwardAction | RoutingChangeAction;

import { Injectable } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { ofType, Actions, Effect } from '@ngrx/effects';
import { filter, map, tap, withLatestFrom } from 'rxjs/operators';

import { State } from '../state';
import { AppSettingsActions } from '../app-settings';
import { ActionTypes, RoutingChangeAction, RoutingGoToAction } from './routing.actions';

@Injectable()
export class RoutingEffects {
  @Effect({ dispatch: false })
  navigate = this.actions.pipe(
    ofType(ActionTypes.ROUTING_GO_TO),
    map((action: RoutingGoToAction) => action.payload),
    tap(({ path, queryParams, extras }) => this.router.navigate(
      path,
      { queryParams, ...extras }
      )
    )
  );

  @Effect({ dispatch: false })
  navigateBack = this.actions.pipe(
    ofType(ActionTypes.ROUTING_GO_BACK),
    withLatestFrom(this.store),
    tap(([action, storeState]) => {
        const index = RoutingEffects.getPreviousIndex(storeState);
        return this.router.navigateByUrl(
          storeState.routing.history[index].urlAfterRedirects
        );
      }
    )
  );

  @Effect({ dispatch: false })
  navigateForward = this.actions.pipe(
    ofType(ActionTypes.ROUTING_GO_FORWARD),
    withLatestFrom(this.store),
    tap(([action, storeState]) => {
        const index = RoutingEffects.getNextIndex(storeState);
        this.router.navigateByUrl(
          storeState.routing.history[index].urlAfterRedirects,
        );
      }
    )
  );

  constructor(
    private actions: Actions,
    private router: Router,
    private store: Store<State>
  ) {
    this.listenToRouter();
  }

  static getPreviousIndex(storeState): number {
    if (storeState.routing.currentIndex === -1) {
      return storeState.routing.history.length - 1;
    }
    return storeState.routing.currentIndex > 1
      ? storeState.routing.currentIndex - 1
      : storeState.routing.currentIndex;
  }

  static getNextIndex(storeState): number {
    if (storeState.routing.currentIndex === -1) {
      return storeState.routing.history.length - 1;
    }
    return storeState.routing.currentIndex > storeState.routing.history.length - 1
      ? storeState.routing.currentIndex + 1
      : storeState.routing.currentIndex;
  }

  listenToRouter() {
    this.router.events
      .pipe(
        filter((event: any) =>
          event instanceof NavigationStart
          || event instanceof NavigationEnd
          || event instanceof NavigationCancel
          || event instanceof NavigationError)
      )
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.store.dispatch(new RoutingChangeAction(event));
        }

        let loadingStatus = false;
        if (event instanceof NavigationStart) {
          loadingStatus = true;
        }

        return this.store.dispatch(
          new AppSettingsActions.SetLoadingStatusAction({
            loadingStatus
          })
        );
      });
  }
}

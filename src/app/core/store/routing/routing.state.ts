import { Route } from '~core/models';

export interface State {
  isFreeNavigation: boolean;
  currentIndex: number;
  currentRoute: Route;
  history: Route[];
}

const initialRoute: Route = {
  id: -1,
  url: '/',
  urlAfterRedirects: '/'
};

export const initialState: State = {
  isFreeNavigation: true,
  currentIndex: -1,
  currentRoute: initialRoute,
  history: [initialRoute]
};

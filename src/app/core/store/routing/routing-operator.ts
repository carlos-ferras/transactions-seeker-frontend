import { Action } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { OperatorFunction } from 'rxjs';

import { ActionTypes, RoutingChangeAction } from './routing.actions';

export function ofRoute(route: string | string[]): OperatorFunction<Action, Action> {
  return filter((action: Action) => {
    const isRouteAction = action.type === ActionTypes.ROUTING_CHANGE;
    if (isRouteAction) {
      const routePath = (action as RoutingChangeAction).payload.urlAfterRedirects;

      return Array.isArray(route)
        ? route.includes(routePath)
        : routePath === route;

    }
    return isRouteAction;
  });
}

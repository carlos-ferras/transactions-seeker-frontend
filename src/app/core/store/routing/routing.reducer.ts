import { Actions, ActionTypes } from './routing.actions';
import { initialState, State } from './routing.state';

export function RoutingReducer(state = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.ROUTING_CHANGE:
      const newState = {
        ...state,
        isFreeNavigation: true,
        currentRoute: action.payload,
        currentIndex: state.isFreeNavigation ? -1 : state.currentIndex
      };
      if (
        (state.currentIndex === -1 || state.isFreeNavigation)
        && (
          state.history.length === 0
          || state.history[state.history.length - 1].urlAfterRedirects !== action.payload.urlAfterRedirects
        )
      ) {
        newState.history = [
          ...state.history,
          action.payload
        ];
      }
      return newState;
    case ActionTypes.ROUTING_GO_FORWARD:
      return {
        ...state,
        isFreeNavigation: false,
        currentIndex: state.currentIndex === -1 || state.currentIndex >= state.history.length - 2
          ? -1
          : state.currentIndex + 1
      };
    case ActionTypes.ROUTING_GO_BACK:
      return {
        ...state,
        isFreeNavigation: false,
        currentIndex: state.currentIndex === -1
          ? state.history.length - 2
          : state.currentIndex - 1
      };
  }
  return state;
}

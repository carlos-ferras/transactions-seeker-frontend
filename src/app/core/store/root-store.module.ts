import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActionReducer, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { storeLogger } from 'ngrx-store-logger';

import { environment } from '~env/environment';
import { AppSettingsModule } from './app-settings';
import { RoutingModule } from './routing';
import { TransactionsModule } from './transactions';

export function logger(reducer: ActionReducer<any>): any {
  return storeLogger()(reducer);
}

export const metaReducers = environment.production ? [] : [logger];

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot(
      {},
      { metaReducers }
    ),
    EffectsModule.forRoot([]),
    AppSettingsModule,
    RoutingModule,
    TransactionsModule
  ],
  exports: [
    AppSettingsModule,
    RoutingModule,
    TransactionsModule
  ]
})
export class RootStoreModule {
}

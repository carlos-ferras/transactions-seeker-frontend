import { TestBed } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { MatSnackBar } from '@angular/material';

import { Actions } from '@ngrx/effects';
import { cold, hot } from 'jest-marbles';
import { Observable } from 'rxjs';
import { configureTestSuite } from 'ng-bullet';
import { TranslateService } from '@ngx-translate/core';

import { provideMagicalMock } from '~app/test-utils';
import { AppSettingsEffects } from './app-settings.effects';
import { SetAvailableLangsAction, SetCurrentLangAction, SetDefaultLangAction } from './app-settings.actions';


export class TestActions extends Actions {
  constructor() {
    super();
  }

  set stream(source: Observable<any>) {
    this.source = source;
  }
}

export function getActions() {
  return new TestActions();
}


describe('AppSettingsEffects', () => {
  let actions: TestActions;
  let effects: AppSettingsEffects;
  let translateService: TranslateService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule
      ],
      providers: [
        AppSettingsEffects,
        {
          provide: Actions,
          useFactory: getActions
        },
        provideMagicalMock(TranslateService),
        provideMagicalMock(MatSnackBar)
      ]
    });
  });

  beforeEach(() => {
    translateService = TestBed.get(TranslateService);
    actions = TestBed.get(Actions);
    effects = TestBed.get(AppSettingsEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should set languages list after SET_AVAILABLE_LANGS action', () => {
    const payload = {
      langs:
        [{
          code: 'en',
          name: 'English'
        }]
    };
    const action = new SetAvailableLangsAction(payload);
    actions.stream = cold('--a', { a: action });

    effects.setAvailableLangsEffect.subscribe(result => {
      expect(result).toEqual(payload);
      expect(translateService.addLangs).toHaveBeenCalledWith(payload.langs);
    });
  });

  it('should set default language after SET_DEFAULT_LANG action', () => {
    const payload = {
      lang: 'en'
    };
    const action = new SetDefaultLangAction(payload);
    const outcome = new SetCurrentLangAction(payload);

    actions.stream = hot('--a', { a: action });
    const expected = cold('--b', { b: outcome });

    effects.setDefaultLangEffect.subscribe(result => {
      expect(result).toEqual(expected.values['b']);
      expect(translateService.setDefaultLang).toHaveBeenCalledWith(payload.lang);
    });
  });

  it('should use browser lang if SET_DEFAULT_LANG payload is empty', () => {
    jest.spyOn(translateService, 'getBrowserLang').mockReturnValue('de');
    const payload = {
      lang: ''
    };
    const action = new SetDefaultLangAction(payload);
    const outcome = new SetCurrentLangAction(payload);

    actions.stream = hot('--a', { a: action });
    const expected = cold('--b', { b: outcome });

    effects.setAvailableLangsEffect.subscribe(result => {
      expect(result).toEqual(expected.values['b']);
      expect(translateService.setDefaultLang).toHaveBeenCalledWith('de');
    });
  });

  it('should set current language after SET_CURRENT_LANG action', () => {
    const payload = {
      lang: 'en'
    };
    const action = new SetCurrentLangAction(payload);
    actions.stream = cold('--a', { a: action });

    effects.setCurrentLangEffect.subscribe(result => {
      expect(result).toEqual(payload);
      expect(translateService.use).toHaveBeenCalledWith(payload.lang);
    });
  });
});

import { Lang } from '../../models';

export interface State {
  availableLangs: Lang[];
  defaultLang: string;
  currentLang: string;
  translations: any;
  isPageLoading: boolean;
  errors: string[];
}

export const initialState: State = {
  defaultLang: '',
  currentLang: '',
  isPageLoading: false,
  errors: [],
  availableLangs: [],
  translations: {}
};

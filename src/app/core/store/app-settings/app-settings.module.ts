import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AppSettingsReducer } from './app-settings.reducer';
import { AppSettingsEffects } from './app-settings.effects';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature('appSettings', AppSettingsReducer),
    EffectsModule.forFeature([AppSettingsEffects])
  ],
  providers: [AppSettingsEffects]
})
export class AppSettingsModule { }

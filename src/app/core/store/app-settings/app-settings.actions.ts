import { Action } from '@ngrx/store';

import { Lang } from '../../models';

export enum ActionTypes {
  SET_AVAILABLE_LANGS = '[App Settings] Set Available Langs',
  SET_DEFAULT_LANG = '[App Settings] Set Default Lang',
  SET_CURRENT_LANG = '[App Settings] Set Current Lang',
  SET_TRANSLATIONS = '[App Settings] Set Translations',
  SET_LOADING_STATUS = '[App Settings] Set Loading Status',
  PUSH_ERROR = '[App Settings] Push Error'
}

export class SetAvailableLangsAction implements Action {
  readonly type = ActionTypes.SET_AVAILABLE_LANGS;

  constructor(public payload: { langs: Lang[] }) {
  }
}

export class SetDefaultLangAction implements Action {
  readonly type = ActionTypes.SET_DEFAULT_LANG;

  constructor(public payload: { lang: string }) {
  }
}

export class SetCurrentLangAction implements Action {
  readonly type = ActionTypes.SET_CURRENT_LANG;

  constructor(public payload: { lang: string }) {
  }
}

export class SetTranslationsAction implements Action {
  readonly type = ActionTypes.SET_TRANSLATIONS;

  constructor(public payload: { translations: any }) {
  }
}

export class SetLoadingStatusAction implements Action {
  readonly type = ActionTypes.SET_LOADING_STATUS;

  constructor(public payload: { loadingStatus: boolean }) {
  }
}

export class PushErrorAction implements Action {
  readonly type = ActionTypes.PUSH_ERROR;

  constructor(public payload: { error: string }) {
  }
}

export type Actions = SetAvailableLangsAction | SetDefaultLangAction | SetCurrentLangAction | SetTranslationsAction | SetLoadingStatusAction | PushErrorAction;

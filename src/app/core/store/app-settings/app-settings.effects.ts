import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

import { map, mergeMap, tap } from 'rxjs/operators';
import { ofType, Actions, Effect } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';

import {
  ActionTypes,
  PushErrorAction,
  SetAvailableLangsAction,
  SetCurrentLangAction,
  SetDefaultLangAction,
  SetTranslationsAction
} from './app-settings.actions';

@Injectable()
export class AppSettingsEffects {
  @Effect({ dispatch: false })
  setAvailableLangsEffect = this.actions
    .pipe(
      ofType(ActionTypes.SET_AVAILABLE_LANGS),
      map((action: SetAvailableLangsAction) => action.payload),
      tap(({ langs }) => {
        this.translateService.addLangs(
          langs.map((lang) => lang.code)
        );
      })
    );

  @Effect()
  setDefaultLangEffect = this.actions
    .pipe(
      ofType(ActionTypes.SET_DEFAULT_LANG),
      map((action: SetDefaultLangAction) => action.payload),
      tap(({ lang }) => {
        if (!lang) {
          const browserLang = this.translateService.getBrowserLang();
          lang = this.translateService.langs.includes(browserLang)
            ? browserLang
            : 'en';
        }
        this.translateService.setDefaultLang(lang);
      }),
      map(({ lang }) => new SetCurrentLangAction({ lang }))
    );

  @Effect()
  setCurrentLangEffect = this.actions
    .pipe(
      ofType(ActionTypes.SET_CURRENT_LANG),
      map((action: SetCurrentLangAction) => action.payload),
      mergeMap(({ lang }) => {
        return this.translateService.use(lang).pipe(map(
          (translations) => new SetTranslationsAction({ translations })
        ));
      })
    );

  @Effect({ dispatch: false })
  showLastErrorEffect = this.actions
    .pipe(
      ofType(ActionTypes.PUSH_ERROR),
      map((action: PushErrorAction) => action.payload),
      tap(({ error }) => this.snackBar.open(
        error,
        null,
        {
          duration: 5000,
          panelClass: 'snack-bar-warn'
        })
      )
    );

  constructor(
    private actions: Actions,
    private snackBar: MatSnackBar,
    private translateService: TranslateService) {
  }
}

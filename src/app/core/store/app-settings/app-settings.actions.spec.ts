import { TestBed } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { MatSnackBar } from '@angular/material';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { configureTestSuite } from 'ng-bullet';
import { TranslateService } from '@ngx-translate/core';

import { provideMagicalMock } from '~app/test-utils';
import { AppSettingsEffects } from './app-settings.effects';
import {
  Actions,
  ActionTypes, PushErrorAction,
  SetAvailableLangsAction,
  SetCurrentLangAction,
  SetDefaultLangAction,
  SetLoadingStatusAction
} from './app-settings.actions';
import { AppSettingsReducer } from './app-settings.reducer';
import { initialState } from './app-settings.state';

describe('AppSettingsActions', () => {
  let effects: AppSettingsEffects;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        StoreModule.forRoot(AppSettingsReducer),
        EffectsModule.forRoot([AppSettingsEffects])
      ],
      providers: [
        AppSettingsEffects,
        provideMagicalMock(TranslateService),
        provideMagicalMock(MatSnackBar),
      ]
    });
  });

  beforeEach(() => {
    effects = TestBed.get(AppSettingsEffects);
  });

  it('should get initial', () => {
    const state = AppSettingsReducer(undefined, {} as Actions);
    expect(state).toEqual(initialState);
  });

  it('should update state on SET_AVAILABLE_LANGS action', () => {
    const payload = {
      langs:
        [{
          code: 'en',
          name: 'English'
        }]
    };

    const action = new SetAvailableLangsAction(payload);
    expect(action).toEqual({
      type: ActionTypes.SET_AVAILABLE_LANGS,
      payload
    });

    const state = AppSettingsReducer(undefined, action);
    expect(state).toEqual({
      ...initialState,
      availableLangs: payload.langs
    });
  });

  it('should update state on SET_DEFAULT_LANG action', () => {
    const payload = {
      lang: 'en'
    };

    const action = new SetDefaultLangAction(payload);
    expect(action).toEqual({
      type: ActionTypes.SET_DEFAULT_LANG,
      payload
    });

    const state = AppSettingsReducer(undefined, action);
    expect(state).toEqual({
      ...initialState,
      defaultLang: payload.lang
    });
  });

  it('should update state on SET_CURRENT_LANG action', () => {
    const payload = {
      lang: 'en'
    };

    const action = new SetCurrentLangAction(payload);
    expect(action).toEqual({
      type: ActionTypes.SET_CURRENT_LANG,
      payload
    });

    const state = AppSettingsReducer(undefined, action);
    expect(state).toEqual({
      ...initialState,
      currentLang: payload.lang
    });
  });

  it('should update state on SET_LOADING_STATUS action', () => {
    const payload = {
      loadingStatus: true
    };

    const action = new SetLoadingStatusAction(payload);
    expect(action).toEqual({
      type: ActionTypes.SET_LOADING_STATUS,
      payload
    });

    const state = AppSettingsReducer(undefined, action);
    expect(state).toEqual({
      ...initialState,
      isPageLoading: payload.loadingStatus
    });
  });

  it('should update state on PUSH_ERROR action', () => {
    const payload = {
      error: 'test'
    };

    const action = new PushErrorAction(payload);
    expect(action).toEqual({
      type: ActionTypes.PUSH_ERROR,
      payload
    });

    const state = AppSettingsReducer(undefined, action);
    expect(state).toEqual({
      ...initialState,
      errors: ['test']
    });
  });
});

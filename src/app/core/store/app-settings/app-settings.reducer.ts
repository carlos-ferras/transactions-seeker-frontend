import { getTranslationsProxy } from '~shared/utils';

import { Actions, ActionTypes } from './app-settings.actions';
import { initialState, State } from './app-settings.state';

export function AppSettingsReducer(state = initialState, action: Actions): State {
  switch (action.type) {
    case ActionTypes.SET_AVAILABLE_LANGS:
      return {
        ...state,
        availableLangs: action.payload.langs
      };
    case ActionTypes.SET_DEFAULT_LANG:
      return {
        ...state,
        defaultLang: action.payload.lang
      };
    case ActionTypes.SET_CURRENT_LANG:
      return {
        ...state,
        currentLang: action.payload.lang
      };
    case ActionTypes.SET_TRANSLATIONS:
      return {
        ...state,
        translations: getTranslationsProxy(action.payload.translations)
      };
    case ActionTypes.SET_LOADING_STATUS:
      return {
        ...state,
        isPageLoading: action.payload.loadingStatus
      };
    case ActionTypes.PUSH_ERROR:
      return {
        ...state,
        errors: [
          action.payload.error,
          ...state.errors
        ]
      };
  }
  return state;
}

import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';

import { Lang } from '../../models';
import { initialState, State } from './app-settings.state';

export const getAvailableLangs = (state: State): Lang[] => state ? state.availableLangs : initialState.availableLangs;
export const getDefaultLang = (state: State): string => state ? state.defaultLang : initialState.defaultLang;
export const getCurrentLang = (state: State): string => state ? state.currentLang : initialState.currentLang;
export const getTranslations = (state: State): any => state ? state.translations : initialState.translations;
export const getIsPageLoading = (state: State): boolean => state ? state.isPageLoading : initialState.isPageLoading;
export const getErrors = (state: State): string[] => state ? state.errors : initialState.errors;
export const getLastError = (state: State): string => state ? state.errors[0] : null;

export const selectAppSettingsState: MemoizedSelector<object, State> = createFeatureSelector<State>('appSettings');

export const selectAvailableLangs: MemoizedSelector<object, Lang[]> = createSelector(
  selectAppSettingsState,
  getAvailableLangs
);

export const selectDefaultLang: MemoizedSelector<object, string> = createSelector(
  selectAppSettingsState,
  getDefaultLang
);

export const selectCurrentLang: MemoizedSelector<object, string> = createSelector(
  selectAppSettingsState,
  getCurrentLang
);

export const selectTranslations: MemoizedSelector<object, any> = createSelector(
  selectAppSettingsState,
  getTranslations
);

export const selectIsPageLoading: MemoizedSelector<object, boolean> = createSelector(
  selectAppSettingsState,
  getIsPageLoading
);

export const selectErrors: MemoizedSelector<object, string[]> = createSelector(
  selectAppSettingsState,
  getErrors
);

export const selectLastError: MemoizedSelector<object, string> = createSelector(
  selectAppSettingsState,
  getLastError
);

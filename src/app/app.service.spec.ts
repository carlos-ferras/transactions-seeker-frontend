import { async, TestBed } from '@angular/core/testing';

import { configureTestSuite } from 'ng-bullet';
import { provideMockStore } from '@ngrx/store/testing';

import { AppService } from './app.service';

describe('AppService', () => {
  let service: AppService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [
        AppService,
        provideMockStore({})
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    service = TestBed.get(AppService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should initialize the app', async(() => {
    jest.spyOn(service, 'initializeI18n');

    service.initializeApp().then(() => {
      expect(service.initializeI18n).toHaveBeenCalled();
    });
  }));
});

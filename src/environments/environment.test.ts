export const environment = {
  production: true,
  testing: true,
  tsApiServer: 'https://test.api',
  tsApiSubscriptions: 'ws://test.api',
  tsLangs: 'en,English de,Deutsch',
  tsDefaultLang: 'en'
};

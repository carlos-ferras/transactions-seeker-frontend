module.exports = function(wallaby) {
  const jestTransform = (file) =>
    require('jest-preset-angular/preprocessor').process(file.content, file.path, {
      globals: {
        __TRANSFORM_HTML__: true
      },
      rootDir: __dirname
    });

  return {
    files: [
      { pattern: 'package.json', load: false },
      { pattern: 'tsconfig.json', load: false },
      { pattern: 'src/tsconfig.spec.json', load: false },
      { pattern: 'src/jest-setup.ts', load: false },
      { pattern: 'jest-global-mocks.ts', load: false },
      { pattern: 'src/**/*.spec.ts', ignore: true },
      { pattern: 'babel.config.js', instrument: false },
      'src/**/*.+(ts|html|json|css|scss|jpg|jpeg|gif|png|svg)'
    ],

    filesWithNoCoverageCalculated: [
      'src/main.ts',
      'src/polyfills.ts',
      'src/wallabyTest.ts',
      'src/jest-global-mocks.ts',
      'src/**/index.ts',
      'src/**/*.module.ts',
      'src/app/test-utils/**/*',
    ],

    tests: ['src/app/**/*.spec.ts'],

    env: {
      type: 'node',
      runner: 'node'
    },

    compilers: {
      '**/*.html': (file) => ({
        code: jestTransform(file),
        map: {
          version: 3,
          sources: [],
          names: [],
          mappings: []
        },
        ranges: []
      })
    },

    preprocessors: {
      'src/**/*.js': [
        jestTransform,
        (file) =>
          require('@babel/core').transform(file.content, {
            sourceMap: true,
            compact: false,
            filename: file.path,
            presets: [require('babel-preset-jest')]
          })
      ]
    },

    testFramework: 'jest',

    debug: true
  };
};

FROM nginx:1.13.9-alpine

ARG TS_API_IP
ARG TS_API_PORT

ENV TS_API_IP ${TS_API_IP}
ENV TS_API_PORT ${TS_API_PORT}

COPY ./dist/ /usr/share/nginx/html

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

RUN sed -i 's/TS_API_IP:TS_API_PORT/'"${TS_API_IP}:${TS_API_PORT}"'/g' /etc/nginx/conf.d/default.conf

CMD ["nginx", "-g", "daemon off;"]
